import java.util.*;

public class AgendaContatos {
    private HashMap<String, Contato> contatos;
    
    public AgendaContatos() {   
        contatos = new LinkedHashMap<>();
    }
    
    public void adicionar(String nome, String telefone) {
        Contato contatoNovo = new Contato(nome, telefone);
        this.contatos.put(nome, contatoNovo);
    }
    
    public String consultarTelefone(String nome) {
        return this.contatos.get(nome).getTelefone();
    }
    
    public String consultarNome(String telefone) {
        for(HashMap.Entry<String, Contato> par : contatos.entrySet()) {
            if(par.getValue().getTelefone().equals(telefone)) {
                return par.getKey();
            }
        }
        return null;
    }
    
    public String csv() {
        StringBuilder mensagem = new StringBuilder();
        String separador = System.lineSeparator();
        for(HashMap.Entry<String, Contato> par : contatos.entrySet()) {
            mensagem.append(par.getValue().getNome());
            mensagem.append(",");
            mensagem.append(par.getValue().getTelefone());
            mensagem.append(separador);
        }
        return mensagem.toString();
    }
    
}
