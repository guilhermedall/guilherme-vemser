

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ExercitoElfoTest {
    @Test
    public void alistarElfo() {
        ElfoVerde elfoVerde = new ElfoVerde("LegolasVerde");
        ExercitoElfo exercito = new ExercitoElfo();
        exercito.alistarElfo(elfoVerde);
        assertEquals(exercito.getAlistados().get(0), elfoVerde);
    }
    
    @Test
    public void buscarElfoFerido() {
        ElfoNoturno elfoNoturno = new ElfoNoturno("LegolasNoturno");
        ElfoVerde elfoVerde = new ElfoVerde("LegolasVerde");
        ExercitoElfo exercito = new ExercitoElfo();
        exercito.alistarElfo(elfoVerde);
        exercito.alistarElfo(elfoNoturno);
        Dwarf novoDwarf = new Dwarf("Gimli");
        elfoNoturno.atirarFlecha(novoDwarf);
        assertEquals(exercito.buscarStatus(Status.FERIDO).get(0), elfoNoturno);
    }
    
    @Test
    public void colocarMesmoElfoDuasVezesEFalhar() {
        ElfoVerde elfoVerde = new ElfoVerde("LegolasVerde");
        ExercitoElfo exercito = new ExercitoElfo();
        exercito.alistarElfo(elfoVerde);
        exercito.alistarElfo(elfoVerde);
        assertEquals(1, exercito.getAlistados().size());
    }
}
