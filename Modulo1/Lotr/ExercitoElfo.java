import java.util.*;

public class ExercitoElfo{
    private final ArrayList<Class> TIPOS_PERMITIDOS = new ArrayList<>(
        Arrays.asList(
            ElfoVerde.class,
            ElfoNoturno.class
        )    
    );
    
    protected ArrayList<Elfo> alistados;
    private HashMap<Status, ArrayList<Elfo>> porStatus = new HashMap<>();
    
    {
        alistados = new ArrayList<>();
    }
    
    public ExercitoElfo() {
        
    }
    
    public ArrayList<Elfo> getAlistados() {
        return this.alistados;
    }
    
    public void alistarElfo(Elfo elfo) {
        if(this.TIPOS_PERMITIDOS.contains(elfo.getClass())) {
            if(!alistados.contains(elfo)) {
                alistados.add(elfo);
                ArrayList<Elfo> elfoDoStatus = porStatus.get(elfo.getStatus());
                if(elfoDoStatus == null) {
                    elfoDoStatus = new ArrayList<>();
                    porStatus.put(elfo.getStatus(), elfoDoStatus);
                }
                elfoDoStatus.add(elfo);
            }
        }   
    }
    
    
    //Com HashMap
    public ArrayList<Elfo> buscarStatus(Status status) {
        return this.porStatus.get(status);
    }
    /*
    public ArrayList<Elfo> buscarStatus(Status status) {
        ArrayList<Elfo> alistadosComStatus = new ArrayList<>();
        for(Elfo elfo : this.alistados) {
            if(elfo.getStatus() == status) {
                alistadosComStatus.add(elfo);
            }
        }
        return alistadosComStatus;
    }
    */
   
    public void ordenaLista() {
        for(Elfo elfo : this.alistados) {
            if(elfo.getClass() == ElfoNoturno.class) {
                alistados.remove(elfo);
                alistados.add(elfo);
            }
        }
    }
}
