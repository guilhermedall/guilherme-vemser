import java.util.*;

public class EstrategiaAtaqueIntercalado implements Estrategia {
    private ArrayList<Elfo> ordenacao(ArrayList<Elfo> elfos) {
        //https://www.mkyong.com/java8/java-8-lambda-comparator-example/
        Collections.sort(elfos, new Comparator<Elfo>(){
            public int compare(Elfo elfoAtual, Elfo proximoElfo){
                boolean tipoDiferente = elfoAtual.getClass() != proximoElfo.getClass();
                if(tipoDiferente) {
                    return 0;
                }
                
                return elfoAtual instanceof ElfoVerde && proximoElfo instanceof ElfoNoturno ? 0 : 1;
            }
        });
        return elfos;
    }    
        
    public ArrayList<Elfo> getOrdemDeAtaque(ArrayList<Elfo> atacantes) {
        return ordenacao(atacantes);
    }
}