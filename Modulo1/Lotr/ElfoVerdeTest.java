

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoVerdeTest
{
    @Test
    public void atirarFlechaDiminuirFlechaAumentarXp() {
        ElfoVerde novoElfoVerde = new ElfoVerde("LegolasVerde");
        Dwarf novoDwarf = new Dwarf("Gimli");
        novoElfoVerde.atirarFlecha(novoDwarf);
        assertEquals(2, novoElfoVerde.getExperiencia());
        assertEquals(1, novoElfoVerde.getQtdFlecha());
    }
    
    @Test
    public void elfoVerdeAdicionarItemComDescricaoValida() {
        ElfoVerde elfoVerde = new ElfoVerde("Celebron");
        Item arcoDeVidro = new Item(1, "Arco de Vidro");
        elfoVerde.ganharItem(arcoDeVidro);
        Inventario inventario = elfoVerde.getInventario();
        assertEquals(new Item(2, "Flecha"), inventario.obter(0));
        assertEquals(new Item(1, "Arco"), inventario.obter(1));
        assertEquals(arcoDeVidro, inventario.obter(2));
    }
    
    @Test
    public void elfoVerdeAdicionarItemComDescricaoInvalida() {
        ElfoVerde elfoVerde = new ElfoVerde("Celebron");
        Item arcoDeMadeira = new Item(1, "Arco de Madeira");
        elfoVerde.ganharItem(arcoDeMadeira);
        Inventario inventario = elfoVerde.getInventario();
        assertEquals(new Item(2, "Flecha"), inventario.obter(0));
        assertEquals(new Item(1, "Arco"), inventario.obter(1));
        assertNull(inventario.buscar("Arco de Madeira"));
    }
    
    @Test
    public void elfoVerdePerdeItemComDescricaoValida() {
        ElfoVerde elfoVerde = new ElfoVerde("Celebron");
        Item arcoDeVidro = new Item(1, "Arco de Vidro");
        elfoVerde.ganharItem(arcoDeVidro);
        elfoVerde.perderItem(arcoDeVidro);
        Inventario inventario = elfoVerde.getInventario();
        assertEquals(new Item(2, "Flecha"), inventario.obter(0));
        assertEquals(new Item(1, "Arco"), inventario.obter(1));
        assertNull(inventario.buscar("Arco de Vidro"));
    }
    
    @Test
    public void elfoVerdePerdeItemComDescricaoInvalida() {
        ElfoVerde elfoVerde = new ElfoVerde("Celebron");
        Item arco = new Item(1, "Arco");
        elfoVerde.ganharItem(arco);
        Inventario inventario = elfoVerde.getInventario();
        assertEquals(new Item(2, "Flecha"), inventario.obter(0));
        assertEquals(new Item(1, "Arco"), inventario.obter(1));
    }
}
