


import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoNoturnoTest
{
    @Test
    public void atirarFlechaDiminuirFlechaAumentarXpDiminuirVidaFicaFerido() {
        ElfoNoturno novoElfoNoturno = new ElfoNoturno( "LegolasNoturno");
        Dwarf novoDwarf = new Dwarf("Gimli");
        novoElfoNoturno.atirarFlecha(novoDwarf);
        assertEquals(3, novoElfoNoturno.getExperiencia());
        assertEquals(1, novoElfoNoturno.getQtdFlecha());
        assertEquals(85.0, novoElfoNoturno.getVida(), 1e-9);
        assertEquals(Status.FERIDO, novoElfoNoturno.getStatus());
    }
    
    @Test
    public void atira7FlechasEMorre() {
        ElfoNoturno novoElfoNoturno = new ElfoNoturno( "LegolasNoturno");
        Dwarf novoDwarf = new Dwarf("Gimli");
        novoElfoNoturno.getFlecha().setQuantidade(100);
        for(int i = 0; i < 8; i++) {
            novoElfoNoturno.atirarFlecha(novoDwarf);
        }
        assertEquals(Status.MORTO, novoElfoNoturno.getStatus());
    }
}
