import java.util.*;

public class ElfoDaLuz extends Elfo{
    private int contAtaque;
    private final ArrayList<String> DESCRICOES_OBRIGATORIAS = new ArrayList<>(
        Arrays.asList(
            "Espada de Galvorn"
        )
    );
    
    {
        contAtaque = 1;
    }
    
    public ElfoDaLuz(String nome) {
        super(nome);
        this.qtdDano = 21;
        super.ganharItem(new ItemSempreExistente(1, DESCRICOES_OBRIGATORIAS.get(0)));
    }
    
    @Override
    public void perderItem(Item item) {
        boolean podePerder = !DESCRICOES_OBRIGATORIAS.contains(item.getDescricao());
        if(podePerder) {
            this.inventario.remover(item);
        }
    }
    
    public boolean deveGanharVida() {
        return this.contAtaque % 2 == 0;
    }
    
    public void atacarComEspada(Dwarf dwarf) {
        this.aumentarXp();
        if(deveGanharVida()) {
            this.ganharVida(10);
        }else {
            this.sofrerDano();
        }
        dwarf.sofrerDano();
        contAtaque++;
    }
}
