import java.util.*;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class InventarioTest {    
    @Test
    public void adicionarUmItem() {
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        inventario.adicionar(espada);
        assertEquals(espada, inventario.obter(0));
    }
    
    @Test
    public void adicionarDoisItens() {
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        inventario.adicionar(espada);
        inventario.adicionar(espada);
        assertEquals(espada, inventario.obter(0));
        assertEquals(espada, inventario.obter(1));
    }
    
    @Test
    public void removerItemPorIndice() {
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        inventario.adicionar(espada);
        inventario.remover(0);
        assertEquals(0, inventario.getSize());
    }
    
    @Test
    public void removerItem() {
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        inventario.adicionar(espada);
        inventario.remover(espada);
        assertEquals(0, inventario.getSize());
    }
    
    @Test
    public void buscaItem() {
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        inventario.adicionar(espada);
        assertEquals(espada, inventario.buscar("Espada"));
    }
    
    @Test
    public void getDescricoesVariosItens() {
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        Item arco = new Item(1, "Arco");
        Item escudo = new Item(1, "Escudo");
        inventario.adicionar(espada);
        inventario.adicionar(arco);
        inventario.adicionar(escudo);
        assertEquals("Espada,Arco,Escudo", inventario.getDescricoesItens());
    }
    
    @Test
    public void getDescricoesVariosItensNenhumItem() {
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        Item arco = new Item(7, "Arco");
        Item escudo = new Item(4, "Escudo");
        inventario.adicionar(espada);
        inventario.adicionar(arco);
        inventario.adicionar(escudo);
        assertEquals(arco, inventario.getItemMaiorQuantidade());
    }
    
    @Test
    public void getItemMaiorQuantidade() {
        Inventario inventario = new Inventario();
        assertEquals("", inventario.getDescricoesItens());
    }
    
    @Test
    public void inverteListaVazia() {
        Inventario inventario = new Inventario(1);
        assertTrue(inventario.inverter().isEmpty());
    }
    
    @Test
    public void retornaListaInvertida() {
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item(1, "Espada"));
        inventario.adicionar(new Item(1, "Escudo"));
        assertEquals("Escudo", inventario.inverter().get(0).getDescricao());
        assertEquals("Espada", inventario.inverter().get(1).getDescricao());
    }
    
    @Test
    public void ordenaInventarioAscendente() {
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        Item arco = new Item(7, "Arco");
        Item escudo = new Item(4, "Escudo");
        inventario.adicionar(espada);
        inventario.adicionar(arco);
        inventario.adicionar(escudo);
        inventario.ordenarItens(TipoOrdenacao.ASC);
        assertEquals(espada, inventario.obter(0));
        assertEquals(escudo, inventario.obter(1));
        assertEquals(arco, inventario.obter(2));
    }
    
    @Test
    public void ordenaInventarioDescendente() {
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        Item arco = new Item(7, "Arco");
        Item escudo = new Item(4, "Escudo");
        inventario.adicionar(espada);
        inventario.adicionar(arco);
        inventario.adicionar(escudo);
        inventario.ordenarItens(TipoOrdenacao.DESC);
        assertEquals(espada, inventario.obter(2));
        assertEquals(escudo, inventario.obter(1));
        assertEquals(arco, inventario.obter(0));
    }
}
