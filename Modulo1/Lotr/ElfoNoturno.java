
public class ElfoNoturno extends Elfo{
    public ElfoNoturno(String nome) {
        super(nome);
        this.xpPorAtaque = 3;
        this.qtdDano = 15;
    }
}
