import java.util.*;

public class ElfoVerde extends Elfo{
    private final ArrayList<String> DESCRICOES_VALIDAS = new ArrayList<>(
        Arrays.asList(
            "Espada de Aço Valiriano", 
            "Arco de Vidro",
            "Flecha de Vidro"
        )
    ); 

    public ElfoVerde(String nome) {
        super(nome);
        this.xpPorAtaque = 2;
    }
    
    @Override
    public void atirarFlecha(Dwarf dwarf) {
        if(this.getQtdFlecha() > 0) {
            this.getFlecha().setQuantidade( this.getQtdFlecha() - 1 );
            this.aumentarXp();
            dwarf.sofrerDano();
        }
    }
    
    @Override
    public void ganharItem(Item item) {
        boolean descricaoValida = DESCRICOES_VALIDAS.contains(item.getDescricao());
        if(descricaoValida) {
            this.inventario.adicionar(item);
        }
    }
    
    @Override
    public void perderItem(Item item) {
        boolean descricaoValida = DESCRICOES_VALIDAS.contains(item.getDescricao());
        if(descricaoValida) {
            this.inventario.remover(item);
        }
    }
}
