import java.util.*;

public class PaginadorInventario
{
    private Inventario inventario;
    private int pontoInicial;
    
    {
        pontoInicial = 0;
    }
    
    public PaginadorInventario(Inventario inventario) {
        this.inventario = inventario;
    }
    
    public void pular(int n) {
        this.pontoInicial = n < 0 ? 0 : n;
    }
    
    public ArrayList<Item> limitar(int n) {
        ArrayList<Item> itens = new ArrayList<>();
        for(int i = this.pontoInicial; i < this.pontoInicial + n && i < this.inventario.getItens().size(); i++) {
            itens.add(this.inventario.obter(i));
        }
        return itens;
    }
}
