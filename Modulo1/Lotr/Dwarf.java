
public class Dwarf extends Personagem {
    private boolean escudoEquipado;
    
    {
        escudoEquipado = false;
    }
    
    public Dwarf(String nome) {
        super(nome);
        this.vida = 110.0;
        this.ganharItem(new Item(1, "Escudo"));
        this.qtdDano = 10.0;
    }
    
    public void equiparEscudo(Item escudo) {
        if(escudo.getDescricao().equals("Escudo")) {
            this.qtdDano = 5.0;
        }
    }
    
    public void desequiparEscudo() {
        this.qtdDano = 10.0;
    }
    
    public String imprimirResumo(){
        return "Dwarf";
    }
}
