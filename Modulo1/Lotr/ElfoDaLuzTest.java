

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoDaLuzTest {
    @Test
    public void ataca2VezesPerdeEGanhaVida() {
        ElfoDaLuz elfoDaLuz = new ElfoDaLuz("Galadriel");
        Dwarf dwarf = new Dwarf("Gimli");
        elfoDaLuz.atacarComEspada(dwarf);
        assertEquals(79, elfoDaLuz.getVida(), 1e-9);
        elfoDaLuz.atacarComEspada(dwarf);
        assertEquals(89, elfoDaLuz.getVida(), 1e-9);
    }
    
    @Test
    public void tentarPerderEspadaEFalhar() {
        ElfoDaLuz elfoDaLuz = new ElfoDaLuz("Galadriel");
        elfoDaLuz.perderItem(elfoDaLuz.inventario.obter(2));
        assertEquals("Espada de Galvorn", elfoDaLuz.inventario.obter(2).getDescricao());
    }
}
