public abstract class Personagem {
    protected String nome;
    protected Status status;
    protected Inventario inventario;
    protected double vida, qtdDano;
    protected int experiencia, xpPorAtaque;
    
    {
        status = Status.RECEM_CRIADO;
        inventario = new Inventario(0);
        experiencia = 0;
        xpPorAtaque = 1;
        qtdDano = 0.0;
    }
    
    public Personagem(String nome) {
        this.nome = nome;
    }
    
    public String getNome() {
        return this.nome;
    }
    
    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public double getVida() {
        return this.vida;
    }
    
    public void ganharVida(double vidaGanha) {
        this.vida += vidaGanha;
    }
    
    public int getExperiencia() {
        return this.experiencia;
    }
    
    public Status getStatus() {
        return this.status;
    }
    
    public Inventario getInventario() {
        return this.inventario;
    }
    
    public void aumentarXp() {
        this.experiencia += xpPorAtaque;
    }
    
    public boolean podeSofrerDano() {
        return this.vida > 0;
    }
    
    public void sofrerDano() {
        if(podeSofrerDano() && this.qtdDano > 0.0) {
            this.vida = this.vida >= this.qtdDano ? vida - this.qtdDano : 0.0;
            if(this.vida == 0) {
                this.status = Status.MORTO;
            }else {
                this.status = Status.FERIDO;
            }
        }
    }
    
    public void ganharItem(Item item) {
        this.inventario.adicionar(item);
    }
    
    public void perderItem(Item item) {
        this.inventario.remover(item);
    }
    
    public abstract String imprimirResumo();
}
