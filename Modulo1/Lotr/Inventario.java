import java.util.*;

public class Inventario {
    private ArrayList<Item> itens;
    
    public Inventario() {
        this.itens = new ArrayList<Item>();
    }
    
    public Inventario(int quantidade) {
        this.itens = new ArrayList<Item>(quantidade);
    }

    public ArrayList<Item> getItens() {
        return this.itens;
    }
    
    public void adicionar(Item item) {
        this.itens.add(item);
    }
    
    public Item obter(int indice) {
        if(indice < 0 || indice >= this.itens.size()) return null;
        return this.itens.get(indice);
    }
    
    public void remover(int indice) {
        this.itens.remove(indice);
    }
    
    public void remover(Item item) {
        this.itens.remove(item);
    }
    
    public int getSize() {
        return this.itens.size();
    }
    
    public String getDescricoesItens() {
        StringBuilder mensagem = new StringBuilder();
        for(int i = 0; i < this.itens.size(); i++) {
            Item item = this.itens.get(i);
            mensagem.append(item.getDescricao());
            mensagem.append(",");
        }
        return mensagem.length() > 0 ? mensagem.substring(0, (mensagem.length() - 1)) : mensagem.toString();
    }
       
    public Item getItemMaiorQuantidade() {
        int indiceMaiorQuantidade = 0, maiorQuantidade = 0;
        for(int i = 0; i < this.itens.size(); i++) {
            if(this.itens.get(i).getQuantidade() > maiorQuantidade){
                indiceMaiorQuantidade = i;
                maiorQuantidade = this.itens.get(i).getQuantidade();
            }
        }
        return this.itens.get(indiceMaiorQuantidade);
    }
    
    /*
    public Item buscar(String descricao) {
        for(int i = 0; i < this.itens.size(); i++) {
            Item item = this.itens.get(i);
            if(item.getDescricao().equals(descricao)) return item;
        }
        return null;
    }
    */
    // Com forEach
    public Item buscar(String descricao) {
        for(Item itemAtual : this.itens) {
            boolean encontrei = itemAtual.getDescricao().equals(descricao);
            if(encontrei) {
                return itemAtual;
            }
        }
        return null;
    }
    
    public int buscarIndiceItem(Item item) {
        for(int i = 0; i < itens.size(); i++) {
            if(itens.get(i).equals(item)) {
                return i;
            }
        }
        return -1;
    }
    
    public ArrayList<Item> inverter() {
        ArrayList<Item> listaInvertida = new ArrayList<Item>(this.itens.size());
        for(int i = this.itens.size() - 1; i >= 0; i--) {
            listaInvertida.add(this.itens.get(i));
        }
        return listaInvertida;
    }
    
    public void ordenarItens() {
        this.ordenarItens(TipoOrdenacao.ASC);
    }
    
    public void ordenarItens(TipoOrdenacao ordenacao) {
        Item aux;
        for(int i = 0; i < this.itens.size() - 1; i++) {
            for(int j = 0; j < this.itens.size() - 1 - i; j++) {
                boolean deveTrocar = ordenacao == TipoOrdenacao.ASC ? 
                    this.itens.get(j).getQuantidade() > this.itens.get(j + 1).getQuantidade() :
                    this.itens.get(j).getQuantidade() < this.itens.get(j + 1).getQuantidade();
                if(deveTrocar) {
                    aux = this.itens.get(j);
                    this.itens.set(j, this.itens.get(j + 1));
                    this.itens.set(j + 1, aux);                    
                }
            }
        }
    }
    
    public Inventario unir(Inventario outroInventario) {
        Inventario inventarioUnido = new Inventario();
        for(Item item : this.itens) {
            inventarioUnido.adicionar(item);
        }
        for(Item item : outroInventario.getItens()) {
            inventarioUnido.adicionar(item);
        }
        return inventarioUnido;
    }
    
    public Inventario diferenciar(Inventario outroInventario) {
        Inventario inventarioDiminuido = new Inventario();
        for(Item item : this.itens) {
            inventarioDiminuido.adicionar(item);
        }
        for(Item item : inventarioDiminuido.getItens()) {
            if(outroInventario.getItens().contains(item)) {
                inventarioDiminuido.remover(item);
            }
        }
        return inventarioDiminuido;
    }
    
    public Inventario cruzar(Inventario outroInventario) {
        Inventario inventarioCruzado = new Inventario();
        for(Item item : this.itens) {
            inventarioCruzado.adicionar(item);
        }
        for(Item item : inventarioCruzado.getItens()) {
            if(!outroInventario.getItens().contains(item)) {
                inventarioCruzado.remover(item);
            }
        }
        return inventarioCruzado;
    }
}
