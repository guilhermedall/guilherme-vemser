

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoTest{
    @After
    public void tearDown() {
        System.gc();
    }
    
    @Test
    public void atirarFlechaDiminuirFlechaAumentarXp() {
        Elfo novoElfo = new Elfo( "Legolas");
        Dwarf novoDwarf = new Dwarf("Gimli");
        novoElfo.atirarFlecha(novoDwarf);
        assertEquals(1, novoElfo.getExperiencia());
        assertEquals(1, novoElfo.getQtdFlecha());
    }

    @Test
    public void atirar5FlechasAteZerar() {
        Elfo novoElfo = new Elfo( "Legolas");
        Dwarf novoDwarf = new Dwarf("Gimli");
        for(int i = 0; i <= 2; i++) {
            novoElfo.atirarFlecha(novoDwarf);
        }
        assertEquals(0, novoElfo.getQtdFlecha());
    }
    
    @Test
    public void atirarFlechaDwarf() {
        Elfo novoElfo = new Elfo( "Legolas");
        Dwarf novoDwarf = new Dwarf("Gimli");
        novoElfo.atirarFlecha(novoDwarf);
        assertEquals(1, novoElfo.getExperiencia());
        assertEquals(1, novoElfo.getQtdFlecha());
        assertEquals(100.0, novoDwarf.getVida(), 1e-9);
    }
    
    @Test
    public void elfoNasceComStatusRecemCriado() {
        Elfo novoElfo = new Elfo("Legolas");
        assertEquals(Status.RECEM_CRIADO, novoElfo.getStatus());
    }
    
    @Test
    public void naoCriaElfoNaoIncrementa() {
        assertEquals(0, Elfo.getNumeroElfos());
    }
    
    @Test
    public void instanciaElfoEConta() {
        Elfo elfo = new Elfo("Legolas");
        assertEquals(1, Elfo.getNumeroElfos());
        ElfoVerde elfoVerde = new ElfoVerde("LegolasVerde");
        assertEquals(2, Elfo.getNumeroElfos());
        ElfoNoturno elfoNoturno = new ElfoNoturno("LegolasVerde");
        assertEquals(3, Elfo.getNumeroElfos());
    }
}
