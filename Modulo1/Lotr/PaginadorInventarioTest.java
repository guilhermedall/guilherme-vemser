
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PaginadorInventarioTest
{
    @Test
    public void pularELimitar() {
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item(1, "Espada"));
        inventario.adicionar(new Item(2, "Escudo de metal"));
        inventario.adicionar(new Item(3, "Poção de HP"));
        inventario.adicionar(new Item(4, "Bracelete"));
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        paginador.pular(0);
        assertEquals("Espada", paginador.limitar(2).get(0).getDescricao());
        assertEquals("Escudo de metal", paginador.limitar(2).get(1).getDescricao());
        paginador.pular(2);
        assertEquals("Poção de HP", paginador.limitar(2).get(0).getDescricao());
        assertEquals("Bracelete", paginador.limitar(2).get(1).getDescricao());
    }
}
