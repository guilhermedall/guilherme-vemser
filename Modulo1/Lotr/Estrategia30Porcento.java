import java.util.*;

public class Estrategia30Porcento implements Estrategia{
    private ArrayList<Elfo> ordenarQtdFlechas(ArrayList<Elfo> elfos) {
        Collections.sort(elfos, new Comparator<Elfo>(){
            public int compare(Elfo elfoAtual, Elfo proximoElfo){
                return elfoAtual.getQtdFlecha() - proximoElfo.getQtdFlecha();
            }
        });
        return elfos;
    }
    
    private ArrayList<Elfo> ordenarNoturnosPrimeiros(ArrayList<Elfo> elfos) {
        //https://www.mkyong.com/java8/java-8-lambda-comparator-example/
        Collections.sort(elfos, new Comparator<Elfo>(){
            public int compare(Elfo elfoAtual, Elfo proximoElfo){
                boolean mesmoTipo = elfoAtual.getClass() == proximoElfo.getClass();
                if(mesmoTipo) {
                    return 0;
                }
                
                return elfoAtual instanceof ElfoNoturno && proximoElfo instanceof ElfoVerde ? -1 : 1;
            }
        });
        return elfos;
    }
    
    private ArrayList<Elfo> somenteVivosEComFlechas(ArrayList<Elfo> elfos) {
        for(Elfo elfo : elfos) {
            if(elfo.getStatus() == Status.MORTO || elfo.getQtdFlecha() == 0) {
                elfos.remove(elfo);
            }
        }
        return elfos;
    }

    public ArrayList<Elfo> getOrdemDeAtaque(ArrayList<Elfo> atacantes) {
        ArrayList<Elfo> pelotao = ordenarNoturnosPrimeiros(somenteVivosEComFlechas(atacantes));
        int qtdInicialElfos = pelotao.size();
        
        for(int i = 0; i < (int)(qtdInicialElfos * 0.3); i++) {
            if(pelotao.get(i).getClass() == ElfoNoturno.class) {
                pelotao.remove(pelotao.get(i));
            }
        }
        
        return ordenarQtdFlechas(pelotao);
    }
}
