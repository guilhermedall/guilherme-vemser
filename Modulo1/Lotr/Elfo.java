
public class Elfo extends Personagem {
    private int indiceFlecha;
    private static int contador;
    
    //Abstracao de codigo para organizacao
    {
        indiceFlecha = 0;
    }
    
    public Elfo(String nome) {
        super(nome);
        this.vida = 100.0;
        this.inventario.adicionar(new Item(2, "Flecha"));
        this.inventario.adicionar(new Item(1, "Arco"));
        Elfo.contador++;
    }
    
    protected void finalize() throws Throwable{
        Elfo.contador--;
    }
    
    public static int getNumeroElfos() {
        return contador;
    }
    
    public Item getFlecha() {
        return this.inventario.obter(indiceFlecha);
    }
    
    public int getQtdFlecha() {
        return this.getFlecha().getQuantidade();
    }
    
    public void atirarFlecha(Dwarf dwarf) {
        if(this.getQtdFlecha() > 0) {
            this.getFlecha().setQuantidade( this.getQtdFlecha() - 1 );
            this.aumentarXp();
            this.sofrerDano();
            dwarf.sofrerDano();
        }
    }
    
    public String imprimirResumo(){
        return "Elfo";
    }
}
