package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;

@Entity
public class TipoConta {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "TIPO_CONTA_SEQ", sequenceName = "TIPO_CONTA_SEQ")
    @GeneratedValue(generator = "TIPO_CONTA_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_TIPO_CONTA", nullable = false)
    private Integer id;

    private  String tipo;
}
