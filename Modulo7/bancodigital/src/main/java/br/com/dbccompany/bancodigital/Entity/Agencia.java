package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;

@Entity
public class Agencia {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "AGENCIA_SEQ", sequenceName = "AGENCIA_SEQ")
    @GeneratedValue(generator = "AGENCIA_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_AGENCIA", nullable = false)
    private Integer id;

    private String nome;

    @ManyToOne
    @JoinColumn(name = "ID_CIDADE", nullable = false)
    private Cidade cidade;

    @ManyToOne
    @JoinColumn(name = "ID_BANCO", nullable = false)
    private Banco banco;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    public Banco getBanco() {
        return banco;
    }

    public void setBanco(Banco banco) {
        this.banco = banco;
    }
}
