package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

public class CidadeXCliente {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "CIDADES_X_CLIENTES_SEQ", sequenceName = "CIDADES_X_CLIENTES_SEQ")
    @GeneratedValue(generator = "CIDADES_X_CLIENTES_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_CIDADE_X_CLIENTE", nullable = false)
    private Integer id;

    @ManyToOne
    @JoinColumn(name="id_cliente", nullable=false)
    private Cliente cliente;

    @ManyToOne
    @JoinColumn(name="id_cidade", nullable=false)
    private Cidade cidade;

    @Enumerated(EnumType.STRING)
    private TipoResidencia tipo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    public TipoResidencia getTipo() {
        return tipo;
    }

    public void setTipo(TipoResidencia tipo) {
        this.tipo = tipo;
    }
}
