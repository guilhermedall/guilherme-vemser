package br.com.dbccompany.bancodigital.Service;

import br.com.dbccompany.bancodigital.Entity.Cliente;
import br.com.dbccompany.bancodigital.Repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ClienteService {
    @Autowired
    private ClienteRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public Cliente salvar(Cliente cliente) {
        return repository.save(cliente);
    }

    @Transactional(rollbackFor = Exception.class)
    public Cliente editar(Cliente cliente, Integer id) {
        cliente.setId(id);
        return repository.save(cliente);
    }

    public List<Cliente> todosClientes() {
        return (List<Cliente>) repository.findAll();
    }

    public Cliente paisEspecifico(Integer id) {
        Optional<Cliente> cliente = repository.findById(id);
        return cliente.get();
    }
}
