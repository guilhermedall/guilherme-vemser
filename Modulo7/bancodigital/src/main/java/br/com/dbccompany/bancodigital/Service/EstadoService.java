package br.com.dbccompany.bancodigital.Service;

import br.com.dbccompany.bancodigital.Entity.Estado;
import br.com.dbccompany.bancodigital.Repository.EstadoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class EstadoService {
    @Autowired
    private EstadoRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public Estado salvar(Estado estado) {
        return repository.save(estado);
    }

    @Transactional(rollbackFor = Exception.class)
    public Estado editar(Estado estado, Integer id) {
        estado.setId(id);
        return repository.save(estado);
    }

    public List<Estado> todosEstados() {
        return (List<Estado>) repository.findAll();
    }

    public Estado estadoEspecifico(Integer id) {
        Optional<Estado> estado = repository.findById(id);
        return estado.get();
    }
}
