package br.com.dbccompany.bancodigital.Controller;

import br.com.dbccompany.bancodigital.Entity.Pais;
import br.com.dbccompany.bancodigital.Service.PaisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("api/pais")
public class PaisController {
    @Autowired
    PaisService service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<Pais> todosPaises(){
        return service.todosPaises();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Pais novoPais(@RequestBody Pais pais) {
        return service.salvar(pais);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Pais editarPais(@PathVariable Integer id, @RequestBody Pais pais) {
        return service.editar(pais, id);
    }
}
