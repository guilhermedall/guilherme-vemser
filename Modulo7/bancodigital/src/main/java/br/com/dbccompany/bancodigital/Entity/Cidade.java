package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;

@Entity
public class Cidade {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "CIDADE_SEQ", sequenceName = "CIDADE_SEQ")
    @GeneratedValue(generator = "CIDADE_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_CIDADE", nullable = false)
    private Integer id;

    private String nome;

    @ManyToOne
    @JoinColumn(name = "ID_ESTADO", nullable = false)
    private Estado estado;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }
}
