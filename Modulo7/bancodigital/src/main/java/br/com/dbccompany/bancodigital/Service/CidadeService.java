package br.com.dbccompany.bancodigital.Service;

import br.com.dbccompany.bancodigital.Entity.Cidade;
import br.com.dbccompany.bancodigital.Repository.CidadeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class CidadeService {
    @Autowired
    private CidadeRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public Cidade salvar(Cidade cidade) {
        return repository.save(cidade);
    }

    @Transactional(rollbackFor = Exception.class)
    public Cidade editar(Cidade cidade, Integer id) {
        cidade.setId(id);
        return repository.save(cidade);
    }

    public List<Cidade> todasCidades() {
        return (List<Cidade>) repository.findAll();
    }

    public Cidade cidadeEspecifica(Integer id) {
        Optional<Cidade> cidade = repository.findById(id);
        return cidade.get();
    }
}
