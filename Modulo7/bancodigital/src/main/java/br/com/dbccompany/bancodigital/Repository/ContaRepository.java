package br.com.dbccompany.bancodigital.Repository;

import br.com.dbccompany.bancodigital.Entity.Agencia;
import br.com.dbccompany.bancodigital.Entity.Conta;
import br.com.dbccompany.bancodigital.Entity.TipoConta;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContaRepository extends CrudRepository<Conta, Integer> {
    List<Conta> findBySaldo(Double saldo);
}
