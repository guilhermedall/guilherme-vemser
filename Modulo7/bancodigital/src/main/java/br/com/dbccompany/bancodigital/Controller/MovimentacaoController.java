package br.com.dbccompany.bancodigital.Controller;

import br.com.dbccompany.bancodigital.Entity.Movimentacao;
import br.com.dbccompany.bancodigital.Service.MovimentacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public class MovimentacaoController {
    @Autowired
    MovimentacaoService service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<Movimentacao> todasMovimentacoes(){
        return service.todasMovimentacoes();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Movimentacao novaMovimentacao(@RequestBody Movimentacao movimentacao) {
        return service.salvar(movimentacao);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Movimentacao editarMovimentacao(@PathVariable Integer id, @RequestBody Movimentacao movimentacao) {
        return service.editar(movimentacao, id);
    }
}
