package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.Embeddable;
import javax.persistence.Id;

@Embeddable
public class ContaXClienteId {
    private Integer contaId;
    private Integer clienteId;

    public  ContaXClienteId(int contaId, int clienteId) {
        this.contaId = contaId;
        this.clienteId = clienteId;
    }
}
