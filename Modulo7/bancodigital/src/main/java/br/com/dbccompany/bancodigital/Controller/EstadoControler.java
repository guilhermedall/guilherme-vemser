package br.com.dbccompany.bancodigital.Controller;

import br.com.dbccompany.bancodigital.Entity.Estado;
import br.com.dbccompany.bancodigital.Service.EstadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("api/estado")
public class EstadoControler {
    @Autowired
    EstadoService service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<Estado> todosEstados(){
        return service.todosEstados();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Estado novoEstado(@RequestBody Estado estado) {
        return service.salvar(estado);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Estado editarEstado(@PathVariable Integer id, @RequestBody Estado estado) {
        return service.editar(estado, id);
    }
}
