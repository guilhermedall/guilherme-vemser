package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;

@Entity
public class Conta {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "CONTA_SEQ", sequenceName = "CONTA_SEQ")
    @GeneratedValue(generator = "CONTA_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_CONTA", nullable = false)
    private Integer id;

    private Double saldo;

    @ManyToOne
    @JoinColumn(name = "ID_AGENCIA", nullable = false)
    private Agencia agencia;

    @ManyToOne
    @JoinColumn(name = "ID_TIPO_CONTA", nullable = false)
    private  TipoConta tipo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getSaldo() {
        return saldo;
    }

    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }

    public Agencia getAgencia() {
        return agencia;
    }

    public void setAgencia(Agencia agencia) {
        this.agencia = agencia;
    }

    public TipoConta getTipo() {
        return tipo;
    }

    public void setTipo(TipoConta tipo) {
        this.tipo = tipo;
    }
}
