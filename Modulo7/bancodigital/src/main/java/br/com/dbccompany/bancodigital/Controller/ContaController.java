package br.com.dbccompany.bancodigital.Controller;

import br.com.dbccompany.bancodigital.Entity.Conta;
import br.com.dbccompany.bancodigital.Service.ContaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/conta")
public class ContaController {
    @Autowired
    ContaService service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<Conta> todasContas(){
        return service.todasContas();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Conta novaConta(@RequestBody Conta conta) {
        return service.salvar(conta);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Conta editarConta(@PathVariable Integer id, @RequestBody Conta conta) {
        return service.editar(conta, id);
    }
}
