package br.com.dbccompany.bancodigital.Service;

import br.com.dbccompany.bancodigital.Entity.Agencia;
import br.com.dbccompany.bancodigital.Repository.AgenciaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class AgenciaService {
    @Autowired
    private AgenciaRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public Agencia salvar(Agencia agencia) {
        return repository.save(agencia);
    }

    @Transactional(rollbackFor = Exception.class)
    public Agencia editar(Agencia agencia, Integer id) {
        agencia.setId(id);
        return repository.save(agencia);
    }

    public List<Agencia> todasAgencias() {
        return (List<Agencia>) repository.findAll();
    }

    public Agencia agenciaEspecifica(Integer id) {
        Optional<Agencia> agencia = repository.findById(id);
        return agencia.get();
    }

}
