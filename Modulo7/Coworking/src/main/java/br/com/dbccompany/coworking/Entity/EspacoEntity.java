package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
public class EspacoEntity {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "ESPACO_SEQ", sequenceName = "ESPACO_SEQ")
    @GeneratedValue(generator = "ESPACO_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_ESPACO", nullable = false)
    private Integer id;

    @Column(nullable = false, unique = true)
    private String nome;

    @Column(name = "QUANTIDADE_PESSOAS", nullable = false)
    private int qtdPessoas;

    @Column(nullable = false)
    private double valor;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getQtdPessoas() {
        return qtdPessoas;
    }

    public void setQtdPessoas(int qtdPessoas) {
        this.qtdPessoas = qtdPessoas;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
