package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
public class EspacoPacoteEntity {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "ESPACO_PACOTE_SEQ", sequenceName = "ESPACO_PACOTE_SEQ")
    @GeneratedValue(generator = "ESPACO_PACOTE_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_ESPACO_PACOTE", nullable = false)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "ID_ESPACO")
    private EspacoEntity espaco;

    @ManyToOne
    @JoinColumn(name = "ID_PACOTE")
    private PacoteEntity pacote;

    @Column(name = "TIPO_CONTRATACAO")
    private TipoContratacao tipoContratacao;

    private int quantidade;

    private int prazo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public EspacoEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoEntity espaco) {
        this.espaco = espaco;
    }

    public PacoteEntity getPacote() {
        return pacote;
    }

    public void setPacote(PacoteEntity pacote) {
        this.pacote = pacote;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public int getPrazo() {
        return prazo;
    }

    public void setPrazo(int prazo) {
        this.prazo = prazo;
    }
}
