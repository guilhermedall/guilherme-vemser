package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
public class ContatoEntity {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "CONTATO_SEQ", sequenceName = "CONTATO_SEQ")
    @GeneratedValue(generator = "CONTATO_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_CONTATO", nullable = false)
    private Integer id;

    @OneToOne
    private TipoContatoEntity tipoContato;

    private String valor;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoContatoEntity getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(TipoContatoEntity tipoContato) {
        this.tipoContato = tipoContato;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
}
