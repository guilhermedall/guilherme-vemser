package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
public class PacoteEntity {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "PACOTE_SEQ", sequenceName = "PACOTE_SEQ")
    @GeneratedValue(generator = "PACOTE_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_PACOTE", nullable = false)
    private Integer id;

    private double valor;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
