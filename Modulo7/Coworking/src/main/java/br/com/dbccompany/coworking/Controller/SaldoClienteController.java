package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.SaldoClienteEntity;
import br.com.dbccompany.coworking.Service.SaldoClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/saldo-cliente")
public class SaldoClienteController {
    @Autowired
    SaldoClienteService service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<SaldoClienteEntity> todosSaldoXClientes(){
        return service.todosSaldoXClientes();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public SaldoClienteEntity novoSaldoXCliente(@RequestBody SaldoClienteEntity saldoXCliente) {
        return service.salvar(saldoXCliente);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public SaldoClienteEntity editarSaldoXCliente(@PathVariable Integer id, @RequestBody SaldoClienteEntity saldoXCliente) {
        return service.editar(saldoXCliente, id);
    }
}
