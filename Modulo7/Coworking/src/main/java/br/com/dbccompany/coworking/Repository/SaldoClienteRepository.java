package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.SaldoClienteEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface SaldoClienteRepository extends CrudRepository<SaldoClienteEntity, Integer> {
    SaldoClienteEntity findByQuantidade(int quantidade);
    SaldoClienteEntity findByVencimento(Date vencimento);

    List<SaldoClienteEntity> findAll();
}
