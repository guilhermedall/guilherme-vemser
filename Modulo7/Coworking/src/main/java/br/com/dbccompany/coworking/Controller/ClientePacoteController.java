package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.ClientePacoteEntity;
import br.com.dbccompany.coworking.Service.ClientePacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/cliente-pacote")
public class ClientePacoteController {
    @Autowired
    ClientePacoteService service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<ClientePacoteEntity> todosClientePacote(){
        return service.todosClientePacotes();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public ClientePacoteEntity novoClientePacote(@RequestBody ClientePacoteEntity clientePacote) {
        return service.salvar(clientePacote);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ClientePacoteEntity editarClientePacote(@PathVariable Integer id, @RequestBody ClientePacoteEntity clientePacote) {
        return service.editar(clientePacote, id);
    }
}
