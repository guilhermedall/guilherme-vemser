package br.com.dbccompany.coworking.Entity;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class SaldoClienteId implements Serializable {
    private Integer espacoId;
    private Integer clienteId;

    public SaldoClienteId(int espacoId, int clienteId) {
        this.espacoId = espacoId;
        this.clienteId = clienteId;
    }

    public SaldoClienteId(){
    }

    public Integer getEspacoId() {
        return espacoId;
    }

    public void setEspacoId(Integer espacoId) {
        this.espacoId = espacoId;
    }

    public Integer getClienteId() {
        return clienteId;
    }

    public void setClienteId(Integer clienteId) {
        this.clienteId = clienteId;
    }
}
