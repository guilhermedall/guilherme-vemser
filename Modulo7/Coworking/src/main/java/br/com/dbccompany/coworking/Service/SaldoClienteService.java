package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.SaldoClienteEntity;
import br.com.dbccompany.coworking.Repository.SaldoClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class SaldoClienteService {
    @Autowired
    private SaldoClienteRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public SaldoClienteEntity salvar(SaldoClienteEntity saldoXCliente) {
        return repository.save(saldoXCliente);
    }

    @Transactional(rollbackFor = Exception.class)
    public SaldoClienteEntity editar(SaldoClienteEntity saldoXCliente, Integer id) {
        //TODO: 25/02/2020
        //saldoXCliente.setId(id);
        return repository.save(saldoXCliente);
    }

    public List<SaldoClienteEntity> todosSaldoXClientes() {
        return (List<SaldoClienteEntity>) repository.findAll();
    }

    public SaldoClienteEntity pagamentoEspecifico(Integer id) {
        Optional<SaldoClienteEntity> pagamento = repository.findById(id);
        return pagamento.get();
    }
}
