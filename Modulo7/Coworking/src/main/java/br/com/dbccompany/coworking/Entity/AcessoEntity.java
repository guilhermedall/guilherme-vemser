package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.Date;

@Entity
public class AcessoEntity {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "ACESSO_SEQ", sequenceName = "ACESSO_SEQ")
    @GeneratedValue(generator = "ACESSO_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_ACESSO", nullable = false)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "ID_CLIENTE_SALDO_CLIENTE")
    private ClienteEntity clienteSaldoCliente;

    @ManyToOne
    @JoinColumn(name = "ID_ESPACO_SALDO_CLIENTE")
    private EspacoEntity espacoSaldoCliente;

    private boolean isEntrada;

    private Date data;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ClienteEntity getClienteSaldoCliente() {
        return clienteSaldoCliente;
    }

    public void setClienteSaldoCliente(ClienteEntity clienteSaldoCliente) {
        this.clienteSaldoCliente = clienteSaldoCliente;
    }

    public EspacoEntity getEspacoSaldoCliente() {
        return espacoSaldoCliente;
    }

    public void setEspacoSaldoCliente(EspacoEntity espacoSaldoCliente) {
        this.espacoSaldoCliente = espacoSaldoCliente;
    }

    public boolean isEntrada() {
        return isEntrada;
    }

    public void setEntrada(boolean entrada) {
        isEntrada = entrada;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }
}
