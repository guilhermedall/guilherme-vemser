package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContratacaoRepository extends CrudRepository<ContratacaoEntity, Integer> {
    ContratacaoEntity findByQuantidade(int quantidade);
    ContratacaoEntity findByDesconto(int desconto);
    ContratacaoEntity findByPrazo(int prazo);

    List<ContratacaoEntity> findAll();
}
