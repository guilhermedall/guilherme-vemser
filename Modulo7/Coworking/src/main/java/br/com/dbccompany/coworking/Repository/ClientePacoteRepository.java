package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ClientePacoteEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClientePacoteRepository extends CrudRepository<ClientePacoteEntity, Integer> {
    ClientePacoteEntity findByQuantidade(Integer quantidade);

    List<ClientePacoteEntity> findAll();
}
