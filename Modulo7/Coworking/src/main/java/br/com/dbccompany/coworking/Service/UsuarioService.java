package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.UsuarioEntity;
import br.com.dbccompany.coworking.Repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class UsuarioService {
    @Autowired
    private UsuarioRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public UsuarioEntity salvar(UsuarioEntity usuario) {

        return repository.save(usuario);
    }

    @Transactional(rollbackFor = Exception.class)
    public UsuarioEntity editar(UsuarioEntity usuario, Integer id) {
        usuario.setId(id);
        return repository.save(usuario);
    }

    public List<UsuarioEntity> todosUsuarios() {
        return (List<UsuarioEntity>) repository.findAll();
    }

    public UsuarioEntity usuarioEspecifico(Integer id) {
        Optional<UsuarioEntity> usuario = repository.findById(id);
        return usuario.get();
    }
}
