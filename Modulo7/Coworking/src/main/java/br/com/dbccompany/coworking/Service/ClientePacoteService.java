package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.ClientePacoteEntity;
import br.com.dbccompany.coworking.Repository.ClientePacoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ClientePacoteService {
    @Autowired
    private ClientePacoteRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public ClientePacoteEntity salvar(ClientePacoteEntity clientePacote) {
        return repository.save(clientePacote);
    }

    @Transactional(rollbackFor = Exception.class)
    public ClientePacoteEntity editar(ClientePacoteEntity clientePacote, Integer id) {
        clientePacote.setId(id);
        return repository.save(clientePacote);
    }

    public List<ClientePacoteEntity> todosClientePacotes() {
        return (List<ClientePacoteEntity>) repository.findAll();
    }

    public ClientePacoteEntity clientePacoteEspecifico(Integer id) {
        Optional<ClientePacoteEntity> clientePacote = repository.findById(id);
        return clientePacote.get();
    }
}
