package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.UsuarioEntity;
import br.com.dbccompany.coworking.Service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/usuario")
public class UsuarioController {
    @Autowired
    UsuarioService service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<UsuarioEntity> todosUsuarios(){
        return service.todosUsuarios();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public UsuarioEntity novoUsuario(@RequestBody UsuarioEntity usuario) {
        return service.salvar(usuario);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public UsuarioEntity editarUsuario(@PathVariable Integer id, @RequestBody UsuarioEntity usuario) {
        return service.editar(usuario, id);
    }
}
