package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.EspacoPacoteEntity;
import br.com.dbccompany.coworking.Service.EspacoPacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/espaco-pacote")
public class EspacoPacoteController {
    @Autowired
    EspacoPacoteService service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<EspacoPacoteEntity> todosEspacoPacotes(){
        return service.todosEspacoPacotes();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public EspacoPacoteEntity novoEspacoPacote(@RequestBody EspacoPacoteEntity espacoPacote) {
        return service.salvar(espacoPacote);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public EspacoPacoteEntity editarEspacoPacote(@PathVariable Integer id, @RequestBody EspacoPacoteEntity espacoPacote) {
        return service.editar(espacoPacote, id);
    }
}
