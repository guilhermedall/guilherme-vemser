package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.EspacoPacoteEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EspacoPacoteRepository extends CrudRepository<EspacoPacoteEntity, Integer> {
    EspacoPacoteEntity findByQuantidade(int quantidade);
    EspacoPacoteEntity findByPrazo(int prazo);

    List<EspacoPacoteEntity> findAll();
}
