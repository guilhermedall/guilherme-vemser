package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
public class PagamentoEntity {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "PAGAMENTO_SEQ", sequenceName = "PAGAMENTO_SEQ")
    @GeneratedValue(generator = "PAGAMENTO_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_PAGAMENTO", nullable = false)
    private Integer id;

    @OneToOne
    @JoinColumn(name = "ID_CLIENTE_PACOTE")
    private ClientePacoteEntity clientePacote;

    @OneToOne
    @JoinColumn(name = "ID_CONTRATACAO")
    private ContatoEntity contato;

    @Column(name = "TIPO_PAGAMENTO")
    private TipoPagamento tipoPagamento;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ClientePacoteEntity getClientePacote() {
        return clientePacote;
    }

    public void setClientePacote(ClientePacoteEntity clientePacote) {
        this.clientePacote = clientePacote;
    }

    public ContatoEntity getContato() {
        return contato;
    }

    public void setContato(ContatoEntity contato) {
        this.contato = contato;
    }

    public TipoPagamento getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamento tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }
}
