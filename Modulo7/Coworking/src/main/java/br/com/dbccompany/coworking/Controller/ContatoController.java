package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Service.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/contato")
public class ContatoController {
    @Autowired
    ContatoService service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<ContatoEntity> todosContatos(){
        return service.todosContatos();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public ContatoEntity novoContato(@RequestBody ContatoEntity contato) {
        return service.salvar(contato);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ContatoEntity editarContato(@PathVariable Integer id, @RequestBody ContatoEntity contato) {
        return service.editar(contato, id);
    }
}
