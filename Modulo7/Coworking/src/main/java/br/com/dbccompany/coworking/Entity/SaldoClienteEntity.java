package br.com.dbccompany.coworking.Entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.util.Date;

@Entity
public class SaldoClienteEntity {
    @EmbeddedId
    @Column(name = "ID_SALDO_CLIENTE", nullable = false)
    private SaldoClienteId saldoClienteId;

    @Column(name = "TIPO_CONTRATACAO", nullable = false)
    private TipoContratacao tipoContratacao;

    @Column(nullable = false)
    private int quantidade;

    @Column(nullable = false)
    private Date vencimento;

    public SaldoClienteId getSaldoClienteId() {
        return saldoClienteId;
    }

    public void setSaldoClienteId(SaldoClienteId saldoClienteId) {
        this.saldoClienteId = saldoClienteId;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public Date getVencimento() {
        return vencimento;
    }

    public void setVencimento(Date vencimento) {
        this.vencimento = vencimento;
    }
}
