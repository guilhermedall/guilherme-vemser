package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.EspacoPacoteEntity;
import br.com.dbccompany.coworking.Repository.EspacoPacoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class EspacoPacoteService {
    @Autowired
    private EspacoPacoteRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public EspacoPacoteEntity salvar(EspacoPacoteEntity espacoPacote) {
        return repository.save(espacoPacote);
    }

    @Transactional(rollbackFor = Exception.class)
    public EspacoPacoteEntity editar(EspacoPacoteEntity espacoPacote, Integer id) {
        espacoPacote.setId(id);
        return repository.save(espacoPacote);
    }

    public List<EspacoPacoteEntity> todosEspacoPacotes() {
        return (List<EspacoPacoteEntity>) repository.findAll();
    }

    public EspacoPacoteEntity espacoPacoteEspecifico(Integer id) {
        Optional<EspacoPacoteEntity> espacoPacote = repository.findById(id);
        return espacoPacote.get();
    }
}
