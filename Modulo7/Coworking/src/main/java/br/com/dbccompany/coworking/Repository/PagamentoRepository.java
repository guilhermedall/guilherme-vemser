package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.PagamentoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PagamentoRepository extends CrudRepository<PagamentoEntity, Integer> {
    List<PagamentoEntity> findAll();
}
