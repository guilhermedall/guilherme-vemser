package br.com.dbccompany.TrabalhoFinal.Entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "CARTAO")
public class Cartao {
	@Id
	@SequenceGenerator(allocationSize = 1, name = "CARTAO_SEQ", sequenceName = "CARTAO_SEQ")
	@GeneratedValue(generator = "CARTAO_SEQ", strategy = GenerationType.SEQUENCE)
	@Column(name = "ID_CARTAO", nullable = false)
	private Integer id;
	
	private String chip;
	
	private Date vencimento;
	
	@ManyToOne
	@JoinColumn(name="id_cliente", nullable=false)
	private Cliente cliente;
	
	@ManyToOne
	@JoinColumn(name="id_bandeira", nullable=false)
	private Bandeira bandeira;
	
	@ManyToOne
	@JoinColumn(name="id_emissor", nullable=false)
	private Emissor emissor;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getChip() {
		return chip;
	}

	public void setChip(String chip) {
		this.chip = chip;
	}

	public Date getVencimento() {
		return vencimento;
	}

	public void setVencimento(Date vencimento) {
		this.vencimento = vencimento;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Bandeira getBandeira() {
		return bandeira;
	}

	public void setBandeira(Bandeira bandeira) {
		this.bandeira = bandeira;
	}

	public Emissor getEmissor() {
		return emissor;
	}

	public void setEmissor(Emissor emissor) {
		this.emissor = emissor;
	}
}
