package br.com.dbccompany.TrabalhoFinal.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "LOJA_X_CREDENCIADOR")
@SequenceGenerator(allocationSize = 1, name = "LOJA_X_CREDENCIADOR_SEQ", sequenceName = "LOJA_X_CREDENCIADOR_SEQ")

public class LojaXCredenciador {
	@Id
	@GeneratedValue(generator = "LOJA_X_CREDENCIADOR_SEQ", strategy = GenerationType.SEQUENCE)
	@Column(name = "ID_LOJA_X_CREDENCIADOR", nullable = false)
	private Integer id;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "id_loja", 
		joinColumns = {
			@JoinColumn(name = "id_loja_x_credenciador")
		},
		inverseJoinColumns = {
			@JoinColumn(name = "id_loja")
		}
	)
	private List<Loja> loja = new ArrayList<>();
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "id_credenciador", 
		joinColumns = {
			@JoinColumn(name = "id_loja_x_credenciador")
		},
		inverseJoinColumns = {
			@JoinColumn(name = "id_credenciador")
		}
	)
	private List<Credenciador> credenciador = new ArrayList<>();

	private Double taxa;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<Loja> getLoja() {
		return loja;
	}

	public void setLoja(List<Loja> loja) {
		this.loja = loja;
	}

	public List<Credenciador> getCredenciador() {
		return credenciador;
	}

	public void setCredenciador(List<Credenciador> credenciador) {
		this.credenciador = credenciador;
	}

	public Double getTaxa() {
		return taxa;
	}

	public void setTaxa(Double taxa) {
		this.taxa = taxa;
	}
}
