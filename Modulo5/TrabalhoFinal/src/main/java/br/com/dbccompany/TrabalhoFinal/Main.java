package br.com.dbccompany.TrabalhoFinal;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import br.com.dbccompany.TrabalhoFinal.Entity.Bandeira;
import br.com.dbccompany.TrabalhoFinal.Entity.Cartao;
import br.com.dbccompany.TrabalhoFinal.Entity.Cliente;
import br.com.dbccompany.TrabalhoFinal.Entity.Credenciador;
import br.com.dbccompany.TrabalhoFinal.Entity.Emissor;
import br.com.dbccompany.TrabalhoFinal.Entity.HibernateUtil;
import br.com.dbccompany.TrabalhoFinal.Entity.Lancamento;
import br.com.dbccompany.TrabalhoFinal.Entity.Loja;
import br.com.dbccompany.TrabalhoFinal.Entity.LojaXCredenciador;

public class Main {
	public static void main(String[] args) {
		Session session = null;
		Transaction transaction = null;
		
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			transaction = session.beginTransaction();
			
			Bandeira bandeira = new Bandeira();
			bandeira.setNome("Master");
			bandeira.setTaxa(0.05);
			
			Cliente cliente = new Cliente();
			cliente.setNome("Guilherme");
			
			Emissor emissor = new Emissor();
			emissor.setNome("Santander");
			emissor.setTaxa(0.02);
			
			Cartao cartao = new Cartao();
			cartao.setChip("ch1pn4c1d4d3");
			cartao.setVencimento(null);
			cartao.setBandeira(bandeira);
			cartao.setCliente(cliente);
			cartao.setEmissor(emissor);
						
			Loja loja = new Loja();
			loja.setNome("Loja da tia");
			List<Loja> lojas = new ArrayList<>();
			lojas.add(loja);
			
			Credenciador credenciador = new Credenciador();
			credenciador.setNome("GetNet");
			List<Credenciador> credenciadores = new ArrayList<>();
			credenciadores.add(credenciador);
			
			LojaXCredenciador lojaXCredenciador = new LojaXCredenciador();
			lojaXCredenciador.setCredenciador(credenciadores);
			lojaXCredenciador.setLoja(lojas);
			lojaXCredenciador.setTaxa(0.07);
			
			Lancamento lancamento = new Lancamento();
			lancamento.setDescricao("Compra de croissant");
			lancamento.setValor(4.5);
			lancamento.setDataCompra(null);
			lancamento.setCartao(cartao);
			lancamento.setLoja(loja);
			lancamento.setEmissor(emissor);
			
			session.save(bandeira);
			session.save(cliente);
			session.save(emissor);
			session.save(cartao);
			session.save(credenciador);
			session.save(loja);
			session.save(lojaXCredenciador);
			session.save(lancamento);
			
			System.out.println("Valor para Credenciador:" + lancamento.getValor() * lojaXCredenciador.getTaxa());
			System.out.println("Valor para Bandeira:" + lancamento.getValor() * bandeira.getTaxa());
			System.out.println("Valor para Emissor:" + lancamento.getValor() * emissor.getTaxa());
			
			transaction.commit();
		}catch (Exception e) {
			if(transaction != null) {
				transaction.rollback();
			}
			System.exit(1);
		}finally {
			System.exit(0);
		}
	}
}
