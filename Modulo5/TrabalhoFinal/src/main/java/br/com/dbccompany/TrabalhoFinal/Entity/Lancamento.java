package br.com.dbccompany.TrabalhoFinal.Entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "LANCAMENTO")
public class Lancamento {
	@Id
	@SequenceGenerator(allocationSize = 1, name = "LANCAMENTO_SEQ", sequenceName = "LANCAMENTO_SEQ")
	@GeneratedValue(generator = "LANCAMENTO_SEQ", strategy = GenerationType.SEQUENCE)
	@Column(name = "ID_LANCAMENTO", nullable = false)
	private Integer id;
	
	private String descricao;
	
	private double valor;
	
	@Column(name = "DATA_COMPRA")
	private Date dataCompra;
	
	@ManyToOne
	@JoinColumn(name="id_cartao", nullable=false)
	private Cartao cartao;
	
	@ManyToOne
	@JoinColumn(name="id_loja", nullable=false)
	private Loja loja;
	
	@ManyToOne
	@JoinColumn(name="id_emissor", nullable=false)
	private Emissor emissor;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public Date getDataCompra() {
		return dataCompra;
	}

	public void setDataCompra(Date dataCompra) {
		this.dataCompra = dataCompra;
	}

	public Cartao getCartao() {
		return cartao;
	}

	public void setCartao(Cartao cartao) {
		this.cartao = cartao;
	}

	public Loja getLoja() {
		return loja;
	}

	public void setLoja(Loja loja) {
		this.loja = loja;
	}

	public Emissor getEmissor() {
		return emissor;
	}

	public void setEmissor(Emissor emissor) {
		this.emissor = emissor;
	}
}
