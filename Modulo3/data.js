let agora = new Date(1993, 2, 15);
const meses = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"]
console.log(agora);
console.log(typeof agora);
console.log(agora.getMonth());
console.log(`Mês escrito: ${meses[agora.getMonth()]}`);
console.log(`Mês formatado: ${agora.getMonth() + 1}`);
console.log(agora.getFullYear());
console.log(agora.getDay());
console.log(agora.getDate());
