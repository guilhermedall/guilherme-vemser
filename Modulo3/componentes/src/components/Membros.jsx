import React from "react";

export default (props) => 
    <React.Fragment>
        {props.nome} {props.sobrenome}
    </React.Fragment>

//somente se for jsx
/* [
    props.nome,
    props.sobrenome
] */


{/* <div>
    Olá, {props.nome} {props.sobrenome} 
</div> */}
