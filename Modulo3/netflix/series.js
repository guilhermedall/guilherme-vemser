var series = JSON.parse('[{"titulo":"Stranger Things","anoEstreia":2016,"diretor":["Matt Duffer","Ross Duffer"],"genero":["Suspense","Ficcao Cientifica","Drama"],"elenco":["Winona Ryder","David Harbour","Finn Wolfhard","Millie Bobby Brown","Gaten Matarazzo","Caleb McLaughlin","Natalia Dyer","Charlie Heaton","Cara Buono","Matthew Modine","Noah Schnapp"],"temporadas":2,"numeroEpisodios":17,"distribuidora":"Netflix"},{"titulo":"Game Of Thrones","anoEstreia":2011,"diretor":["David Benioff","D. B. Weiss","Carolyn Strauss","Frank Doelger","Bernadette Caulfield","George R. R. Martin"],"genero":["Fantasia","Drama"],"elenco":["Peter Dinklage","Nikolaj Coster-Waldau","Lena Headey","Emilia Clarke","Kit Harington","Aidan Gillen","Iain Glen ","Sophie Turner","Maisie Williams","Alfie Allen","Isaac Hempstead Wright"],"temporadas":7,"numeroEpisodios":67,"distribuidora":"HBO"},{"titulo":"The Walking Dead","anoEstreia":2010,"diretor":["Jolly Dale","Caleb Womble","Paul Gadd","Heather Bellson"],"genero":["Terror","Suspense","Apocalipse Zumbi"],"elenco":["Andrew Lincoln","Jon Bernthal","Sarah Wayne Callies","Laurie Holden","Jeffrey DeMunn","Steven Yeun","Chandler Riggs ","Norman Reedus","Lauren Cohan","Danai Gurira","Michael Rooker ","David Morrissey"],"temporadas":9,"numeroEpisodios":122,"distribuidora":"AMC"},{"titulo":"Band of Brothers","anoEstreia":20001,"diretor":["Steven Spielberg","Tom Hanks","Preston Smith","Erik Jendresen","Stephen E. Ambrose"],"genero":["Guerra"],"elenco":["Damian Lewis","Donnie Wahlberg","Ron Livingston","Matthew Settle","Neal McDonough"],"temporadas":1,"numeroEpisodios":10,"distribuidora":"HBO"},{"titulo":"The JS Mirror","anoEstreia":2017,"diretor":["Lisandro","Jaime","Edgar"],"genero":["Terror","Caos","JavaScript"],"elenco":["Daniela Amaral da Rosa","Antônio Affonso Vidal Pereira da Rosa","Gustavo Lodi Vidaletti","Bruno Artêmio Johann Dos Santos","Márlon Silva da Silva","Izabella Balconi de Moura","Diovane Mendes Mattos","Luciano Maciel Figueiró","Igor Ceriotti Zilio","Alexandra Peres","Vitor Emanuel da Silva Rodrigues","Raphael Luiz Lacerda","Guilherme Flores Borges","Ronaldo José Guastalli","Vinícius Marques Pulgatti"],"temporadas":1,"numeroEpisodios":40,"distribuidora":"DBC"},{"titulo":"10 Days Why","anoEstreia":2010,"diretor":["Brendan Eich"],"genero":["Caos","JavaScript"],"elenco":["Brendan Eich","Bernardo Bosak"],"temporadas":10,"numeroEpisodios":10,"distribuidora":"JS"},{"titulo":"Mr. Robot","anoEstreia":2018,"diretor":["Sam Esmail"],"genero":["Drama","Techno Thriller","Psychological Thriller"],"elenco":["Rami Malek","Carly Chaikin","Portia Doubleday","Martin Wallström","Christian Slater"],"temporadas":3,"numeroEpisodios":32,"distribuidora":"USA Network"},{"titulo":"Narcos","anoEstreia":2015,"diretor":["Paul Eckstein","Mariano Carranco","Tim King","Lorenzo O Brien"],"genero":["Documentario","Crime","Drama"],"elenco":["Wagner Moura","Boyd Holbrook","Pedro Pascal","Joann Christie","Mauricie Compte","André Mattos","Roberto Urbina","Diego Cataño","Jorge A. Jiménez","Paulina Gaitán","Paulina Garcia"],"temporadas":3,"numeroEpisodios":30,"distribuidora":null},{"titulo":"Westworld","anoEstreia":2016,"diretor":["Athena Wickham"],"genero":["Ficcao Cientifica","Drama","Thriller","Acao","Aventura","Faroeste"],"elenco":["Anthony I. Hopkins","Thandie N. Newton","Jeffrey S. Wright","James T. Marsden","Ben I. Barnes","Ingrid N. Bolso Berdal","Clifton T. Collins Jr.","Luke O. Hemsworth"],"temporadas":2,"numeroEpisodios":20,"distribuidora":"HBO"},{"titulo":"Breaking Bad","anoEstreia":2008,"diretor":["Vince Gilligan","Michelle MacLaren","Adam Bernstein","Colin Bucksey","Michael Slovis","Peter Gould"],"genero":["Acao","Suspense","Drama","Crime","Humor Negro"],"elenco":["Bryan Cranston","Anna Gunn","Aaron Paul","Dean Norris","Betsy Brandt","RJ Mitte"],"temporadas":5,"numeroEpisodios":62,"distribuidora":"AMC"}]')
console.log(series)

Array.prototype.invalida = function(){
    let invalidoPorAno = this.filter(serie => serie.anoEstreia > new Date().getFullYear());
    let invalidoPorPropriedade = this.filter(serie => {
        if(Object.values(serie).includes(null, undefined)) {
            return serie
        }
    })
    return invalidoPorPropriedade.concat(invalidoPorAno).map(serie => serie.titulo).toString();
}

Array.prototype.filtrarPorAno = function(ano){
    return this.filter(serie => serie.anoEstreia >= ano);
}

Array.prototype.procurarPorNome = function(nome){
    let achouNome = false;

    this.find(serie => serie.elenco.forEach(ator => {
        if(ator.includes(nome)) {
            achouNome = true;
        }
    }));
    
    return achouNome;
}

Array.prototype.mediaDeEpisodios = function(){;
    let qtdEpisodios = this.map(serie => serie.numeroEpisodios).
                            reduce((soma, valorAtual) => soma + valorAtual);
    return parseFloat(qtdEpisodios/this.length);
}

Array.prototype.totalSalarios = function(indice){
    function imprimirBRLOneLine(valor) {
        return parseFloat(valor.toFixed(2)).toLocaleString('pt-BR', {
          style: 'currency',
          currency: 'BRL'
        })
      }
    const salarioDiretores = 100000;
    const salarioAtores = 40000;
    let qtdDiretores =  this[indice].diretor.length;
    let qtdAtores =  this[indice].elenco.length;

    return imprimirBRLOneLine(qtdDiretores * salarioDiretores + qtdAtores * salarioAtores) 
}

Array.prototype.queroGenero = function(genero){
    return this.filter(serie => serie.genero.includes(genero)).map(serie => serie.titulo);
}

Array.prototype.queroTitulo = function(titulo){
    return this.filter(serie => serie.titulo.includes(titulo)).map(serie => serie.titulo);
}

Array.prototype.creditos = function(indice){
    let diretoresOrdenados = this[indice].diretor.sort(ordenarPorSobrenome)
    let elencoOrdenado = this[indice].elenco.sort(ordenarPorSobrenome)
    let texto = 
    `   Título    
    ${this[indice].titulo}

    Diretores
    ${diretoresOrdenados}
    
    Elenco
    ${elencoOrdenado}`
    return texto;
}

function ordenarPorSobrenome(nome1, nome2) {
    let split1 = nome1.split(" ");
    let split2 = nome2.split(" ");
    let sobrenome1 = split1[split1.length - 1];
    let sobrenome2 = split2[split2.length - 1];

    if (sobrenome1 < sobrenome2) return -1;
    if (sobrenome1 > sobrenome2) return 1;
    return 0;
}

Array.prototype.nomesAbreviados = function(){
    let estaAbreviadoArray =  this.map(serie => serie.elenco).
                                   map(elenco => elenco.map(ator => temAbreviacao(ator)));
    let i = 0;
    let indiceSerieEscolhida;
    estaAbreviadoArray.forEach(serie => {
        if(!serie.includes(false)) {
            indiceSerieEscolhida = i;
        }
        i++;
    });
    let serieEscolhida = this[indiceSerieEscolhida];
    let palavra = "" 
    serieEscolhida.elenco.forEach(ator => {
        nomeAtor = ator.split(" ");
        palavra += nomeAtor[1];
    });
    return palavra;
 }      

function temAbreviacao(nome) {
    let nomeSeparado = nome.split(" ");
    let padrao = /[A-Z]{1}\./
    return padrao.test(nomeSeparado[1])
}   