//fazer função adicionar

/* function adicionar(num1) {
    return function(num2) {
        return num1 + num2;
    }
} */

let adicionar = num1 => num2 => num1 + num2;

console.log(adicionar(4)(5));
console.log(adicionar(3)(4));
console.log(adicionar(5642)(8749));
