//criar função somaPares

function somaPares(array) {
    let soma = 0;
    for(let i = 0; i < array.length; i++) {
        if(i%2 == 0) {
            soma += array[i];
        }
    }
    return soma;
}
console.log(somaPares([1, 56, 4.34, 6, -2]));
