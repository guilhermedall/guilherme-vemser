//criar função calcularCirculo

let circulo1 = {
    raio: 3,
    tipoCalculo: "A"
};

let circulo2 = {
    raio: 3,
    tipoCalculo: "C"
};

/* function calcularCirculo(circulo) {
    if(circulo.tipoCalculo == "A") {
        return Math.PI * Math.pow(circulo.raio, 2);
    } else if(circulo.tipoCalculo == "C") {
        return 2 * Math.PI * circulo.raio;
    }
} */

function calcularCirculo({raio, tipoCalculo: tipo}) {
    return tipo == "A" ? Math.PI * Math.pow(circulo.raio, 2) : 2 * Math.PI * raio;
}

console.log(calcularCirculo(circulo1));

console.log(calcularCirculo(circulo2));