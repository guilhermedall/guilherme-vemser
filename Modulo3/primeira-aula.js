var nomeDaVariavel = "ValorDaVar";

let nomeDaLet = "1";

//console.log(nomeDaLet);

nomeDaLet++;

//console.log(nomeDaLet);

const nomeDaConst = "ValorDaConst";

const nomeDaConst2 = {
    nome: "Marcos",
    idade: 29
};

//console.log(nomeDaConst2);

function somar() {

}

function somar(valor1, valor2 = 1) {
    console.log(valor1 + valor2);
}

//somar(1);
//somar(1, 2);

function ondeMoro(cidade) {
    //console.log("Eu moro em " + cidade);
    console.log(`Eu moro em ${cidade}`);
}

//ondeMoro("Porto Alegre");

function fruteira() {
    let texto = "Banana" + "\n"
                + "Ameixa" + "\n"
                + "Goiaba" + "\n"
                + "Pêssego" + "\n";
    let newTexto = `
            Banana
                Ameixa
                    Goiaba
                        Pêssego
                    `;
    console.log(texto);
    console.log(newTexto);
}

//fruteira();

let eu = {
    nome: "Guilherme",
    idade: 26,
    altura: 1.68
};

function quemSou(pessoa) {
    console.log(`Meu nome é ${pessoa.nome}, tenho ${pessoa.idade} anos e ${pessoa.altura}m de altura`);
}

//quemSou(eu);

let funcaoSomarValores = function(a, b) {
    return a + b;
}

let add = funcaoSomarValores;
let resultado = add(3, 5);
//console.log(resultado);

const { nome, idade } = eu

//console.log(nome);
//console.log(idade);

const array = [1, 3, 4, 8];
const [n1, , n3, n4, n5 = 18] = array;

console.log(n1, n5, n4);

function testarPessoa( {nome, idade, altura} ) {
    console.log(nome, idade, altura);
}

testarPessoa(eu);

let a1 = 42;
let b1 = 15;

console.log("a1 = " + a1);
console.log("b1 = " + b1);

[a1, b1] =  [b1, a1];

console.log("a1 = " + a1);
console.log("b1 = " + b1);