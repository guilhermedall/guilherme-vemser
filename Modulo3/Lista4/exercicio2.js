let inputName = document.getElementById("nome");
inputName.addEventListener("blur", () => {
    if(inputName.value.length < 10) {
        alert("É necessário ter mais de 10 caracteres no campo Nome");
    }
});

let inputEmail = document.getElementById("email");
inputEmail.addEventListener("blur", () => {
    if(!inputEmail.value.includes("@")) {
        alert("Por favor digite um e-mail válido");
    }
});

let inputPhone = document.getElementById("phone");
let inputMessage = document.getElementById("message");
let buttonSend = document.getElementById("sendButton");

buttonSend.addEventListener("click", () => {
    if(inputName.value.length == 0 ||
       inputEmail.value.length == 0 ||
       inputPhone.value.length == 0 ||
       inputMessage.value.length == 0) {

        alert("Nenhum campo pode estar em branco");
        event.preventDefault();
    }
});