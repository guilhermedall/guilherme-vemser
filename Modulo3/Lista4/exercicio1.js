/* Criar uma função que receba um multiplicador e vários valores a serem multiplicados, 
o retorno deve ser um array! */

function multiplicar(multiplicador, ...valores) {
    return valores.map(valor => multiplicador * valor);
}

console.log(multiplicar(3, 4, 5 ,6))
console.log(multiplicar(6, 4, 5 ,6))