export const login = () => {
    localStorage.setItem("login", 'LoggedIn');
}

export const logout = () => {
    localStorage.removeItem("login");
}

export const isLogin = () => {
    if (localStorage.getItem("login")) {
        return true;
    }

    return false;
}