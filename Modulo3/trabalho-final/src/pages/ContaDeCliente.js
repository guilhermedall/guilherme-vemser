import React, {Component} from "react";
import Header from '../components/Header'

export default class ContaDeCliente extends Component {
    render() {
        const {conta} = this.props.location.state
        return (
            <div className="container">
                <Header/>
                <div className="page">
                    <h2>Contas de Clientes</h2>
                    <h3>Conta {conta.tipo.nome}, <span>ID: {conta.tipo.id}</span></h3>
                    <p>{conta.cliente.nome}, ID: {conta.cliente.id}</p>
                    <p>CPF: {conta.cliente.cpf}</p>
                </div>
            </div>   
        )
    }
}