import React, {Component} from "react";
import Header from '../components/Header'

export default class Agencia extends Component {
    render() {
        const {agencia} = this.props.location.state
        console.log(this.state)
        return (
            <div className="container">
                <Header/>
                <div className="page">
                    <h2>Agências</h2>
                    <h3>{agencia.nome}</h3>
                    <p>Id: {agencia.id}</p>
                    <div>
                        <h4>Endereço:</h4>
                        <p>Rua: {agencia.endereco.logradouro}, {agencia.endereco.numero}</p>
                        <p>Bairro: {agencia.endereco.bairro}</p>
                    </div>
                </div>
            </div>   
        )
    }
}