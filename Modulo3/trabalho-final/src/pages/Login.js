import React, { Component } from 'react';
import axios from 'axios';
import {login} from '../utils/logHelper'

export default class Login extends Component {
    constructor(props) {
        super(props);
        this.handleChangeUser = this.handleChangeUser.bind(this)
        this.handleChangePassword = this.handleChangePassword.bind(this)
        this.logar = this.logar.bind(this)
        this.state = {
            email: "",
            senha: "",
            mensagemErro: ""
        }
    }

    handleChangeUser(evt) {
        this.setState({email: evt.target.value});
    }

    handleChangePassword(evt) {
        this.setState({senha: evt.target.value});
    }

    logar = () => {
        axios.post(`http://localhost:1337/login`, {
            email: this.state.email,
            senha: this.state.senha
        })
        .then(res => {
            console.log(res);
            console.log(res.data);
            login();
            console.log(localStorage.getItem("login"));
            this.props.history.push('/home');;
        })
        .catch(error => {
            console.log(error)
            this.setState({mensagemErro: "Por favor, verifique seu e-mail e senha e tente novamente"})
        })
    }

    render() {
        return (
        <div className="container">
            <div className="login-card">
                <h1>Banco VemSer DBC</h1>
                <p className="mensagem-erro">{this.state.mensagemErro}</p>
                <label for="user-name">Usuário</label>
                <input className="login-input" name="user-name" placeholder="Insira seu e-mail" type="text" onChange={this.handleChangeUser}/>
                <label for="password">Senha</label>
                <input className="login-input" name="password" placeholder="Insira sua senha" type="password" onChange={this.handleChangePassword}/>
                <button className="login-button" onClick={this.logar}>Login</button>
            </div>
        </div>
        );
    }
  
}