import React, { Component } from 'react';
import axios from 'axios';
import {BrowserRouter as Router, Route, Link} from 'react-router-dom';
import InputFilter from '../components/InputFilter'
import Header from '../components/Header'

export default class Clientes extends Component {
    constructor(props) {
        super(props)
        this.filtrarContas = this.filtrarContas.bind(this)
        this.state = {
            listaDeContas: [],
            contasFiltradas: []
        }
    }

    componentWillMount() {
        axios.get(`http://localhost:1337/conta/clientes`, {
            headers :{
                'Authorization': 'banco-vemser-api-fake' 
            }
        })
        .then(res => {
            console.log(res);
            console.log(res.data.cliente_x_conta)
            this.setState({
                listaDeContas: res.data.cliente_x_conta
            });
            this.setState({
                contasFiltradas: this.state.listaDeContas
            });
        })
    }   

    filtrarContas(nome) {
        const contasFiltradas = this.state.listaDeContas.filter(conta => conta.cliente.nome.includes(nome))
        this.setState({
            contasFiltradas: contasFiltradas
        });
    }

    render() {
        let contas = this.state.contasFiltradas.map(conta => 
            <li key={conta.id}>
                <h3><Link to={{pathname: "/contas/conta-cliente", state: {conta}}}>{conta.tipo.nome}</Link></h3>
                <p>Cliente: {conta.cliente.nome}</p>
            </li>
        )
        return (
            <div className="container">
                <Header/>
                <div className="page">
                    <h2>Contas de Clientes</h2>
                    <InputFilter botao={true} funcaoBotao={this.filtrarContas} placeholder="Filtrar por nome">Filtrar</InputFilter>
                    <ul>
                        {contas}
                    </ul>
                </div>
            </div>
        );
    } 
}