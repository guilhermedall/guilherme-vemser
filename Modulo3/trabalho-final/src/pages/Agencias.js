import React, { Component } from 'react';
import axios from 'axios';
import {BrowserRouter as Router, Route, Link} from 'react-router-dom';
import InputFilter from '../components/InputFilter'
import Header from '../components/Header'

export default class Agencias extends Component {
    constructor(props) {
        super(props)
        this.filtrarAgencias = this.filtrarAgencias.bind(this)
        this.filtrarAgenciasDigitais = this.filtrarAgenciasDigitais.bind(this)
        this.resetarFiltro = this.resetarFiltro.bind(this)
        this.state = {
            listaDeAgencias: [],
            agenciasFiltradas: [],
            somenteDigitais: false
        }
    }

    componentWillMount() {
        axios.get(`http://localhost:1337/agencias`, {
            headers: {
                'Authorization': 'banco-vemser-api-fake' 
            }
        })
        .then(res => {
            console.log(res);
            this.setState({
                listaDeAgencias: res.data.agencias,
            });
            this.setState({
                agenciasFiltradas: this.state.listaDeAgencias
            });
        })
    }

    tornarDigital = (agencia) => {
        agencia.is_digital ? agencia.is_digital = false : agencia.is_digital = true;
        console.log(agencia)
    }
    
    filtrarAgencias(nome) {
        const agenciasFiltradas = this.state.listaDeAgencias.filter(agencia => agencia.nome.includes(nome))
        this.setState({
            agenciasFiltradas: agenciasFiltradas
        });
    }

    filtrarAgenciasDigitais() {
        const agenciasFiltradas = this.state.listaDeAgencias.filter(agencia => agencia.is_digital)
        this.setState({
            agenciasFiltradas: agenciasFiltradas,
            somenteDigitais: true
        });
    }

    resetarFiltro() {
        const agenciasFiltradas = this.state.listaDeAgencias
        this.setState({
            agenciasFiltradas: agenciasFiltradas,
            somenteDigitais: false
        });
    }

    render() {
        let agencias = this.state.agenciasFiltradas.map(agencia => 
            <li key={agencia.id}>
                <h3><Link to={{ pathname: "/agencias/agencia", state: {agencia}}}>{agencia.nome}</Link></h3>
                <span>Tornar digital? <input type="checkbox" onClick={this.tornarDigital.bind(this, agencia)}/></span>
            </li>
        )
        return (
            <div className="container">
                <Header/>
                <div className="page">
                    <h2>Agências</h2>
                    <p><button onClick={this.state.somenteDigitais ? this.resetarFiltro : this.filtrarAgenciasDigitais}>{this.state.somenteDigitais ? "Todas agências" : "Somente digitais"}</button></p>
                    <InputFilter botao={true} funcaoBotao={this.filtrarAgencias} placeholder="Filtrar por nome">Filtrar</InputFilter>
                    <ul>
                        {agencias}
                    </ul>
                </div>
            </div>
        );
    } 
}