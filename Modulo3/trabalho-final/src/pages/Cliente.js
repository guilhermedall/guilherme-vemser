import React, {Component} from "react";
import Header from '../components/Header'

export default class Cliente extends Component {
    render() {
        const {cliente} = this.props.location.state
        const agencia = cliente.agencia
        return (
            <div className="container">
                <Header/>
                <div className="page">
                    <h2>Clientes</h2>
                    <h3>{cliente.nome}</h3>
                    <p>ID: {cliente.id}</p>
                    <p>CPF: {cliente.cpf}</p>
                    <div>
                        <h4>Nome da agencia: {agencia.nome}, <span>Id: {agencia.id}</span></h4>
                        <div>
                            <p>Endereço:</p>
                            <p>Rua: {agencia.endereco.logradouro}, {agencia.endereco.numero}</p>
                            <p>Bairro: {agencia.endereco.bairro}</p>
                        </div>  
                    </div>
                </div>
            </div>   
        )
    }
}