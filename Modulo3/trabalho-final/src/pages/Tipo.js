import React, {Component} from "react";
import Header from '../components/Header'

export default class Tipo extends Component {
    render() {
        const {tipo} = this.props.location.state
        return (
            <div className="container">
                <Header/>
                <div className="page">
                    <h2>Tipos</h2>
                    <h3>{tipo.nome}</h3>
                    <p>ID: {tipo.id}</p>  
                </div>
            </div>   
        )
    }
}