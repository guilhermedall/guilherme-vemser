import React, { Component } from 'react';
import Header from '../components/Header'

export default class Home extends Component {
    render() {

        return (
            <div className="container">
                <Header/>
                <div className="page">
                    <h2>Home</h2>
                    <p>Seja bem vindo ao Banco VemSer DBC!</p>
                    <p>Utilize o cabeçalho para navegar entre as páginas.</p>
                </div>
            </div>
        );
      } 
}