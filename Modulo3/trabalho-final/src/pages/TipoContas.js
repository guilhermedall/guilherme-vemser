import React, { Component } from 'react';
import axios from 'axios';
import {BrowserRouter as Router, Route, Link} from 'react-router-dom';
import InputFilter from '../components/InputFilter'
import Header from '../components/Header'

export default class TipoContas extends Component {
    constructor(props) {
        super(props)
        this.filtrarTipos = this.filtrarTipos.bind(this)
        this.state = {
            tiposDeConta: [],
            tiposFiltrados: []
        }
    }

    componentWillMount() {
        axios.get(`http://localhost:1337/tipoContas`, {
            headers :{
                'Authorization': 'banco-vemser-api-fake' 
            }
        })
        .then(res => {
            console.log(res.data);
            this.setState({
                tiposDeConta: res.data.tipos
            });
            this.setState({
                tiposFiltrados: this.state.tiposDeConta
            });
        })
    }
    
    filtrarTipos(nome) {
        const tiposFiltrados = this.state.tiposDeConta.filter(tipo => tipo.nome.includes(nome))
        this.setState({
            tiposFiltrados: tiposFiltrados
        });
    }

    render() {
        let tipos = this.state.tiposFiltrados.map(tipo => 
            <li key={tipo.id}>
                <h3><Link to={{ pathname: "/tipo-contas/tipo", state: {tipo} }}>{tipo.nome}</Link></h3>
            </li>
        )
        return (
            <div className="container">
                <Header/>
                <div className="page">
                    <h2>Tipos</h2>
                    <InputFilter botao={true} funcaoBotao={this.filtrarTipos} placeholder="Filtrar por tipo">Filtrar</InputFilter>
                    <ul>
                        {tipos}
                    </ul>
                </div>
            </div>
        );
    } 
}