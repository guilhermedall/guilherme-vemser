import React, { Component } from 'react';
import axios from 'axios';
import {BrowserRouter as Router, Route, Link} from 'react-router-dom';
import InputFilter from '../components/InputFilter'
import Header from '../components/Header'

export default class Clientes extends Component {
    constructor(props) {
        super(props)
        this.filtrarClientes = this.filtrarClientes.bind(this)
        this.state = {
            listaDeClientes: [],
            clientesFiltrados: []
        }
    }

    componentWillMount() {
        axios.get(`http://localhost:1337/clientes`, {
            headers :{
                'Authorization': 'banco-vemser-api-fake' 
            }
        })
        .then(res => {
            console.log(res);
            this.setState({
                listaDeClientes: res.data.clientes
            });
            this.setState({
                clientesFiltrados: this.state.listaDeClientes
            });
        })
    }   

    filtrarClientes(nome) {
        const clientesFiltrados = this.state.listaDeClientes.filter(cliente => cliente.nome.includes(nome))
        this.setState({
            clientesFiltrados: clientesFiltrados
        });
    }

    render() {
        let clientes = this.state.clientesFiltrados.map(cliente => 
            <li key={cliente.id}>
                <h3><Link to={{ pathname: "/clientes/cliente", state: {cliente} }}>{cliente.nome}</Link></h3>
            </li>
        )
        return (
            <div className="container">
                <Header/>
                <div className="page">
                    <h2>Clientes</h2>
                    <InputFilter botao={true} funcaoBotao={this.filtrarClientes} placeholder="Filtrar por nome">Filtrar</InputFilter>
                    <ul>
                        {clientes}
                    </ul>
                </div>
            </div>
        );
    } 
}