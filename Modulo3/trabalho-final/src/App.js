import React, {Component} from 'react';
import {BrowserRouter as Router, Route, Link} from 'react-router-dom';
import Login from './pages/Login'
import Home from './pages/Home'
import Agencias from './pages/Agencias'
import Agencia from './pages/Agencia'
import Clientes from './pages/Clientes'
import Cliente from './pages/Cliente'
import TipoContas from './pages/TipoContas'
import Tipo from './pages/Tipo'
import ContasDeClientes from './pages/ContasDeClientes'
import ContaDeCliente from './pages/ContaDeCliente'
import PrivateRoute from './components/PrivateRoute';
import PublicRoute from './components/PublicRoute';

import './css/grid.css'
import './css/general.css'
import './css/page.css'
import './css/header.css'
import './css/login.css'

export default class App extends Component {

  render() {    
    return (
      <Router>
        <PublicRoute restricted={true}  path="/" exact component={Login} />
        <PrivateRoute path="/home" exact component={Home} />
        <PrivateRoute path="/agencias" exact component={Agencias} />
        <PrivateRoute path="/agencias/agencia" exact component={Agencia} />
        <PrivateRoute path="/clientes" exact component={Clientes} />
        <PrivateRoute path="/clientes/cliente" exact component={Cliente} />
        <PrivateRoute path="/tipo-contas" exact component={TipoContas} />
        <PrivateRoute path="/tipo-contas/tipo" exact component={Tipo} />
        <PrivateRoute path="/contas" exact component={ContasDeClientes} />
        <PrivateRoute path="/contas/conta-cliente" exact component={ContaDeCliente} />
      </Router>
    );
  }
} 

