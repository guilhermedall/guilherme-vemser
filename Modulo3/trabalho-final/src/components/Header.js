import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {logout} from '../utils/logHelper'
import logo from '../img/logo-dbc-topo.png'
import '../css/header.css'

export default class Header extends Component {
    handleLogout = () =>{
        logout();
    }

    render() {
        return (
            <header className="main-header">
                <nav className="container clearfix">
                    <Link className="logo" to="/">
                        <img src={logo} alt="DBC Logo"/>
                    </Link>
                    <label className="mobile-menu" for="mobile-menu">
                        <span></span>
                        <span></span>
                        <span></span>
                    </label>
                    <input id="mobile-menu" type="checkbox"/>

                    <ul className="clearfix">
                        <li>
                            <Link className="link" to="/home">Home</Link>
                        </li>
                        <li>
                            <Link to="/agencias">Agências</Link>
                        </li>
                        <li>
                            <Link to="/clientes">Clientes</Link>
                        </li>
                        <li>
                            <Link to="/tipo-contas">Tipos de conta</Link>
                        </li>
                        <li>
                            <Link to="/contas">Contas de clientes</Link>
                        </li>
                        <li>
                            <Link onClick={this.handleLogout} to="/">Sair</Link>
                        </li>
                    </ul>
                </nav>
            </header>
        )
    }
}
    