/* eslint-disable no-undef */
const pokeApi = new PokeApi();
let currentPokemon;
let generatedNumbers = [];
const id = document.getElementById( 'input-id' );
const btnSorte = document.getElementById( 'btn-sorte' );
const labelError = document.getElementById( 'error-message' );
const pictureArea = document.getElementById( 'picture-area' );
const typeColors = {
  normal: '#A8A77A',
  fire: '#EE8130',
  water: '#6390F0',
  electric: '#F7D02C',
  grass: '#7AC74C',
  ice: '#96D9D6',
  fighting: '#C22E28',
  poison: '#A33EA1',
  ground: '#E2BF65',
  flying: '#A98FF3',
  psychic: '#F95587',
  bug: '#A6B91A',
  rock: '#B6A136',
  ghost: '#735797',
  dragon: '#6F35FC',
  dark: '#705746',
  steel: '#B7B7CE',
  fairy: '#D685AD',
};

// eslint-disable-next-line eqeqeq
if ( JSON.parse( window.localStorage.getItem( 'numbers' ) ) != undefined ) {
  generatedNumbers = JSON.parse( window.localStorage.getItem( 'numbers' ) );
}

function changeBackgroundColor( types ) {
  const type1 = types[0].type.name;
  let type2 = type1;
  const color1 = typeColors[type1];
  let color2 = color1;
  if ( types.length > 1 ) {
    type2 = types[1].type.name;
    color2 = typeColors[type2];
  }
  pictureArea.style.background = `-webkit-linear-gradient(-0deg, ${ color1 } 0%,${ color2 } 100%)`;
}

function createChart( pokemon ) {
  const arr = [];
  pokemon.stats.map( x => arr.push( x.base_stat ) );
  drawChart( ...arr );
}

function renderPokemon( pokemon ) {
  const labelID = document.getElementById( 'label-pokemon-id' );
  const labelName = document.getElementById( 'label-pokemon-name' );
  const pictureFront = document.getElementById( 'picture-front' );
  const pictureBack = document.getElementById( 'picture-back' );
  const labelHeight = document.getElementById( 'label-pokemon-height' );
  const labelWeight = document.getElementById( 'label-pokemon-weight' );
  const labelTypes = document.getElementById( 'label-pokemon-types' );
  pictureFront.src = pokemon.front_picture;
  pictureBack.src = ( pokemon.back_picture == null ) ? pokemon.front_picture : pokemon.back_picture;
  changeBackgroundColor( pokemon.types );
  labelID.innerHTML = `#${ pokemon.id }`;
  labelName.innerHTML = `${ pokemon.name }`;
  labelHeight.innerHTML = `Altura: ${ pokemon.height * 10 }cm`;
  labelWeight.innerHTML = `Peso: ${ pokemon.weight / 10 }kg`;
  labelTypes.innerHTML = `Tipos: ${ pokemon.types.map( x => `<li> ${ x.type.name } </li>` ).join( '' ) } `;
  createChart( pokemon );
  labelError.innerHTML = '';
}

async function getPokemon( pokemonID ) {
  if ( currentPokemon === undefined || pokemonID !== currentPokemon.id ) {
    await pokeApi.search( pokemonID )
      .then( ( data ) => {
        currentPokemon = new Pokemon( data );
        renderPokemon( currentPokemon );
      } )
      .catch( () => {
        labelError.innerHTML = 'Por favor digite um ID válido!';
      } );
  }
}

id.addEventListener( 'blur', () => getPokemon( id.value ) );

btnSorte.addEventListener( 'click', () => {
  let randomNumber = Math.floor( Math.random() * 802 ) + 1;

  while ( generatedNumbers.includes( randomNumber ) ) {
    randomNumber = Math.floor( Math.random() * 802 ) + 1;
  }
  if ( generatedNumbers.length >= 801 ) {
    generatedNumbers = [];
  }
  getPokemon( randomNumber );
  generatedNumbers.push( randomNumber );
  window.localStorage.setItem( 'numbers', JSON.stringify( generatedNumbers ) );
} );
