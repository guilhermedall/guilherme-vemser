export default class Episodio {
    constructor(nome, duracao, temporada, ordemEpisodio, thumbUrl) {
        this.nome = nome
        this.duracao = duracao
        this.temporada = temporada
        this.ordem = ordemEpisodio
        this.url = thumbUrl
        this.qtdVezesAssistido = 0
    }

    get duracaoEmMin() {
        return `${this.duracao} min`
    }

    get temporadaEpisodio() {
        return `S${this.temporada.toString().padStart(2, "0")}
                E${this.ordem.toString().padStart(2, "0")}`
    }

    marcarComoAssistido() {
        this.assistido = true
    }

    darNota(valor) {
        valor = parseInt(valor);
        if(valor >= 1 && valor <= 5) {
            this.nota = parseInt(valor);
        } 
    }
}