import Avaliacao from './avaliacao';

export default class ListaDeAvaliacoes {
    constructor() {
        this.lista = []
    }

    novaAvaliacao(nomeDoEpisodio, nota) {
        let avaliacao = new Avaliacao(nomeDoEpisodio, nota)
        this.lista.push(avaliacao)
    }

    getLista() {
        return this.lista
    }
}