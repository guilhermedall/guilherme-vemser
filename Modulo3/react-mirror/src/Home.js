import React, { Component } from 'react';
import {BrowserRouter as Router, Route, Link} from 'react-router-dom';


export default class Home extends Component {
    render() {

        return (
            <div>
            <h2>Home</h2>
            <Link className="button" to="/jsflix">JSFlix</Link>
            <Link className="button" to="/mirror">Mirror</Link>
          </div>
        );
      }
}