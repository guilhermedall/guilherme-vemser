import React, {Component} from "react";

export default class ListaDeAvaliacoesUI extends Component {
    render() {
        const {lista} = this.props
        const exibicaoLista = lista.map(avaliacao => 
            <li>
                <h2>{ avaliacao.nomeDoEpisodio }</h2>
                <span>{avaliacao.nota ? `Nota: ${avaliacao.nota}` : ""}</span>
            </li>
        )
        return (
            <ul>
                {exibicaoLista}
            </ul>
        )
    }
}