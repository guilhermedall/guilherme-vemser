import React, {Component} from "react";

export default class EpisodioUi extends Component {
    render() {
        const {episodio} = this.props
        return (
            <React.Fragment>
                <h2>{ episodio.nome }</h2>
                <img src = { episodio.url } alt = { episodio.nome }></img>
                <span>{episodio.assistido ? "Já assistido" : "Não assistido"}</span>
                <span>{episodio.nota ? `Nota: ${episodio.nota}` : ""}</span>
                <span>{ episodio.temporadaEpisodio }</span>
                <span>Duração: { episodio.duracaoEmMin }</span>
            </React.Fragment>
        )
    }
}