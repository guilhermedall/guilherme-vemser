import React, {Component} from "react";
import '../css/mensagem-flash.css'

export default class Input extends Component {
    constructor(props) {
        super(props)
        this.idsTimeouts = []
    }

    fechar = () => {
        this.props.atualizarMensagem(false)
    }

    limparTimeouts() {
        this.idsTimeouts.forEach(clearTimeout)
    }

    componentWillUnmount() {
        this.limparTimeouts()
    }

    componentDidUpdate(prevPros) {
        const {deveExibirMensagem} = this.props
        if(prevPros.deveExibirMensagem !== deveExibirMensagem) {
            const novoIdTimeout = setTimeout(() => {
                this.fechar()
            }, 3000)
            this.idsTimeouts.push(novoIdTimeout)
        }
    }

    render() {
        return (
            <React.Fragment>
                <span className={`mensagem-flash ${this.props.corDeFundo}` }>{this.props.children}</span>
            </React.Fragment>   
        )
    }
}