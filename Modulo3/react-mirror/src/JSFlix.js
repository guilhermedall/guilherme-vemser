import React, { Component } from 'react';
import {BrowserRouter as Router, Route, Link} from 'react-router-dom';
import ListaSeries from './models/ListaSeries';
import Input from './components/Input';

export default class JSFlix extends Component {
    constructor( props ) {
        super( props )
        this.listaSeries = new ListaSeries()
        this.mostrarInvalidas = this.mostrarInvalidas.bind( this )
        this.filtrarPorAno = this.filtrarPorAno.bind( this )
        this.procurarPorNome = this.procurarPorNome.bind( this )
        this.exibirMediaDeEpisodios = this.exibirMediaDeEpisodios.bind( this )
        this.exibirTotalSalario = this.exibirTotalSalario.bind( this )
        this.filtrarPorGenero = this.filtrarPorGenero.bind( this )
        this.filtrarPorTitulo = this.filtrarPorTitulo.bind( this )
        this.exibirCreditos = this.exibirCreditos.bind( this )
        this.exibirMsgSecreta = this.exibirMsgSecreta.bind( this )
        this.state = {
            msgInvalidas: "",
            msgFiltroAno: "",
            msgFiltroNome: "",
            msgMediaEpisodios: "",
            msgTotalSalarios: "",
            msgFiltroGenero: "",
            msgFiltroTitulo: "",
            msgCreditos: "",
            msgSecreta:""
        }
      }

    mostrarInvalidas() {
        const msgInvalidas = this.listaSeries.invalidas()
        this.setState({
            msgInvalidas
        }) 
    }

    filtrarPorAno(ano) {
        const msgFiltroAno = this.listaSeries.filtrarPorAno(ano)
        this.setState({
            msgFiltroAno
        }) 
    }

    procurarPorNome(nome) {
        const msgFiltroNome = this.listaSeries.procurarPorNome(nome)
        this.setState({
            msgFiltroNome
        }) 
    }

    exibirMediaDeEpisodios() {
        const msgMediaEpisodios = this.listaSeries.mediaDeEpisodios()
        this.setState({
            msgMediaEpisodios
        }) 
    }

    exibirTotalSalario(indice) {
        const msgTotalSalarios = this.listaSeries.totalSalarios(indice)
        this.setState({
            msgTotalSalarios
        }) 
    }

    filtrarPorGenero(genero) {
        const msgFiltroGenero = this.listaSeries.queroGenero(genero)
        this.setState({
            msgFiltroGenero
        }) 
    }

    filtrarPorTitulo(titulo) {
        const msgFiltroTitulo = this.listaSeries.queroTitulo(titulo)
        this.setState({
            msgFiltroTitulo
        }) 
    }

    exibirCreditos(indice) {
        const msgCreditos = this.listaSeries.creditos(indice)
        this.setState({
            msgCreditos
        }) 
    }

    exibirMsgSecreta(indice) {
        const msgSecreta = this.listaSeries.nomesAbreviados()
        this.setState({
            msgSecreta
        }) 
    }


    render() {
        
        return (
            <div>
                <h2>JSFlix</h2>
                <Link className="button" to="/">Home</Link>
                <Link className="button" to="/mirror">Mirror</Link>
                <section className="container">
                    <div className="row invalidas">
                        <span >{this.state.msgInvalidas}</span>
                        <button onClick={this.mostrarInvalidas}>Invalidas</button>
                    </div>
                    <div className="row">
                        <span>{this.state.msgFiltroAno}</span>
                        <Input botao={true} funcaoBotao={this.filtrarPorAno}>Filtar por Ano</Input>
                    </div>
                    <div className="row">
                        <span>{this.state.msgFiltroNome}</span>
                        <Input botao={true} funcaoBotao={this.procurarPorNome}>Sou Ator</Input>
                    </div>
                    <div className="row">
                        <span >{this.state.msgMediaEpisodios}</span>
                        <button onClick={this.exibirMediaDeEpisodios}>Media de Episodios</button>
                    </div>
                    <div className="row">
                        <span>{this.state.msgTotalSalarios}</span>
                        <Input botao={true} funcaoBotao={this.exibirTotalSalario}>Total Salário</Input>
                    </div>
                    <div className="row">
                        <span>{this.state.msgFiltroGenero}</span>
                        <Input botao={true} funcaoBotao={this.filtrarPorGenero}>Filtrar por Genero</Input>
                    </div>
                    <div className="row">
                        <span>{this.state.msgFiltroTitulo}</span>
                        <Input botao={true} funcaoBotao={this.filtrarPorTitulo}>Filtrar por Titulo</Input>
                    </div>
                    <div className="row">
                        <span>{this.state.msgCreditos}</span>
                        <Input botao={true} funcaoBotao={this.exibirCreditos}>Exibir Creditos</Input>
                    </div>
                    <div className="row">
                        <span >{this.state.msgSecreta}</span>
                        <button onClick={this.exibirMsgSecreta}>Mensagem Secreta</button>
                    </div>
                </section>
            </div>
        );
      }
}