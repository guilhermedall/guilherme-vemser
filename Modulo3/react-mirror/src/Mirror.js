import React, { Component } from 'react';
import ListaEpisodios from './models/listaEpisodios';
import ListaDeAvaliacoes from './models/listaDeAvaliacoes';
import ListaDeAvaliacoesUI from '../src/components/listaDeAvaliacoesUI';
import EpisodioUi  from './components/episodioUi'
import MensagemFlash  from './components/MensagemFlash'
import {BrowserRouter as Router, Route, Link} from 'react-router-dom';
import './App.css';



class Mirror extends Component{
  constructor( props ) {
    super( props )
    this.listaEpisodios = new ListaEpisodios()
    this.listaDeAvaliacoes = new ListaDeAvaliacoes()
    this.sortear = this.sortear.bind( this )
    this.assistido = this.assistido.bind(this)
    this.avaliar = this.avaliar.bind(this)
    this.state = {
      episodio: this.listaEpisodios.episodiosAleatorios,
      msgAviso: "",
      corDoFlash: "verde"
    };
  }

  sortear() {
    const episodio = this.listaEpisodios.episodiosAleatorios
    this.setState({ 
      episodio
    })
  }

  assistido() {
    const {episodio} = this.state
    episodio.marcarComoAssistido()
    this.setState({
      episodio
    })
  }

  avaliar(nota) {
    const {episodio} = this.state
    episodio.darNota(nota)
    nota = parseInt(nota)
    
    if(nota >= 1 && nota <= 5) {
      this.trocarCorFlash("verde")
      episodio.nota = nota;
      this.listaDeAvaliacoes.novaAvaliacao(episodio.nome, episodio.nota);
      const msgAviso = "Nota registrada com sucesso!"
      this.setState({
        msgAviso
      })
    } else {
      this.trocarCorFlash("vermelho")
      const msgAviso = "Por favor, coloque um valor de 1 a 5"
      this.setState({
        msgAviso
      })
    }

    setTimeout(() => {
      const msgAviso = ""
      this.setState({
        msgAviso
      })
    }, 3000);

    this.setState({
      episodio
    }) 
  }

  geraCampoDeNota() {
    return (
      this.state.episodio.assistido && (
        <input placeholder = "Nota de 1 a 5" onBlur = { evento => this.avaliar(evento.target.value) }></input>
      )
    )
  }

  trocarCorFlash(cor) {
    const corDoFlash = cor;
    this.setState({
      corDoFlash
    }) 
  }

  render() {

    const { episodio } = this.state
    
    return (
      <div className = "App">
        {/* <span>{this.state.msgAviso} </span> */}
        <MensagemFlash corDeFundo={this.state.corDoFlash}>{this.state.msgAviso}</MensagemFlash>
        <header className =" App-header"> 
          <Link to="/">Home</Link>
          <Link to="/jsflix">JSFlix</Link>
          <EpisodioUi episodio={episodio}/>
          {this.geraCampoDeNota()}
          <button onClick = { this.sortear }>Trocar Episodio</button>
          <button onClick = { this.assistido }>Já Assisti</button>
          <input onBlur = {evt => this.trocarCorFlash(evt.target.value)}></input>
        </header>
        <ListaDeAvaliacoesUI lista={this.listaDeAvaliacoes.getLista()}/>
      </div>
    );
  }
}

export default Mirror;

/* function teste() {
  console.log("teste");
}

export {teste}; */