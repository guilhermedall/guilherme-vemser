import React, { Component } from 'react';
import {BrowserRouter as Router, Route, Link} from 'react-router-dom';
import '../src/css/grid.css';
import '../src/css/button.css';
import Mirror from './Mirror';
import Home from './Home';
import JSFlix from './JSFlix';

export default class App extends Component {

  render() {    
    return (
      <Router>
        <Route path="/" exact component={Home} />
        <Route path="/mirror" component={ Mirror } />
        <Route path="/jsflix" component={ JSFlix } />
      </Router>
    );
  }
} 
