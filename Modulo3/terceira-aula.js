function criarSanduiche(pao, recheio, queijo, salada) {
    /* console.log(`
    Seu sanduiche tem o pão ${pao}
    com recheio de ${recheio}
    e queijo ${queijo} com
    salada de ${salada}`); */
}

const ingredientes = ["3 queijos", "frango", "cheddar", "tomate e alface"];

criarSanduiche(...ingredientes);

function receberValoresIndefinidos(...valores) {
    /* valores.map(valor => console.log(valor)); */
}

receberValoresIndefinidos(1,2,3,4,5,6);

/* console.log(..."Marcos"); */

//Window or Document
let inputTeste = document.getElementById("campoTeste")
inputTeste.addEventListener("blur", () => {
    alert("Obrigado");
});