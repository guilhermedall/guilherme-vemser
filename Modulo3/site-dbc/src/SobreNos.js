import React, { Component } from 'react';
import Header from './components/Header';
import Footer from './components/Footer';
import ArtigoSobreNos from './components/ArtigoSobreNos';
import './css/reset.css'
import './App.css'; 
import './css/grid.css'
import './css/banner.css'

export default class SobreNos extends Component {

  render() {
    return (
      <div className="App">
        <Header/>

       <section className="container about-us">
        <ArtigoSobreNos posicaoFoto="esquerda"/>
        <ArtigoSobreNos posicaoFoto="direita"/>
        <ArtigoSobreNos posicaoFoto="esquerda"/>
        <ArtigoSobreNos posicaoFoto="direita"/>
       </section>
       
       <Footer/>
      </div>
    );
  }
  
}
