import React, { Component } from 'react';
import Header from './components/Header';
import DefaultButton from './components/DefaultButton';
import ProfileBox from './components/ProfileBox';
import Footer from './components/Footer';
import './css/reset.css'
import './App.css'; 
import './css/grid.css'
import './css/banner.css'

class Home extends Component {

  render() {
    return (
      <div className="App">
        <Header/>
        <section className="main-banner">
           <article>
                <h1>Vem ser DBC</h1>
                <p>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. 
                    Dolore, similique. Eveniet facere nobis harum! Commodi, illo eveniet? 
                    Quibusdam ex, recusandae rem quod temporibus ullam, numquam, 
                    vero a asperiores consequatur blanditiis.
                </p>
                <DefaultButton buttonClass="button-green" text="Saiba Mais"/>
           </article>
       </section>
       <section class="container">
        <div class="row">
            <article class="col col-12 col-md-7">
                <h2>Título</h2>
                <p>
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
                    Magnam molestias ea dolorum odio magni sed et reprehenderit eos, 
                    culpa voluptatum. Quibusdam recusandae quod, quaerat qui ducimus natus 
                    autem a expedita? Lorem, ipsum dolor sit amet consectetur adipisicing elit. 
                    Blanditiis itaque odit eaque excepturi beatae nesciunt sed accusamus ad at 
                    debitis deleniti accusantium, est quasi dolores molestiae temporibus 
                    voluptatibus iusto dolor.
                </p>
            </article>
            <article class="col col-12 col-md-5">
                <h2>Título</h2>
                <p>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Magni quod 
                    aliquam nemo facere doloribus facilis necessitatibus odit perferendis, 
                    cum rem eaque sunt. Enim doloribus reprehenderit asperiores perferendis 
                    blanditiis dignissimos odit.
                </p>
            </article>
        </div>
       </section>
       <section className="container">
         <div className="row">
          <div class="col col-12 col-md-6 col-lg-3">
            <ProfileBox/>
          </div>
          <div class="col col-12 col-md-6 col-lg-3">
            <ProfileBox/>
          </div>
          <div class="col col-12 col-md-6 col-lg-3">
            <ProfileBox/>
          </div>
          <div class="col col-12 col-md-6 col-lg-3">
            <ProfileBox/>
          </div>
         </div>
       </section>
       
       <Footer/>
      </div>
    );
  }
  
}

export default Home;
