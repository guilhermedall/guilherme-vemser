import React, { Component } from 'react';
import Header from './components/Header';
import Footer from './components/Footer';
import ServiceBox from './components/ServiceBox';
import './css/reset.css'
import './App.css'; 
import './css/grid.css'
import './css/banner.css'

class Home extends Component {

  render() {
    return (
      <div className="App">
        <Header/>
        
       <section className="container">
         <div className="row">
          <div class="col col-12 col-md-6 col-lg-4">
            <ServiceBox/>
          </div>
          <div class="col col-12 col-md-6 col-lg-4">
            <ServiceBox/>
          </div>
          <div class="col col-12 col-md-6 col-lg-4">
            <ServiceBox/>
          </div>
         </div>
         <div className="row">
          <div class="col col-12 col-md-6 col-lg-4">  
            <ServiceBox/>
          </div>
          <div class="col col-12 col-md-6 col-lg-4">
            <ServiceBox/>
          </div>
          <div class="col col-12 col-md-6 col-lg-4">
            <ServiceBox/>
          </div>
         </div>
       </section>
    
       <Footer/>
      </div>
    );
  }
  
}

export default Home;
