import React, { Component } from 'react';
import Header from './components/Header';
import Footer from './components/Footer';
import ContactForm from './components/ContactForm';
import Map from './components/Map';
import './css/reset.css'
import './App.css'; 
import './css/grid.css'
import './css/banner.css'

class Home extends Component {  

  render() {
    return (
      <div className="App">
        <Header/>
        
       <section className="container">
        <div className="row">
          <div class="col col-12 col-md-6">
            <h2>Título</h2>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto sit 
                quidem in illo doloribus? Placeat esse repellat pariatur consectetur 
                voluptates nemo, voluptatum sapiente expedita? Ratione soluta ex veritatis? 
                Blanditiis, minima?
            </p>
            <ContactForm/>
          </div>
          <div class="col col-12 col-md-6">
            <Map />
          </div>
        </div>
       </section>
       <Footer/>
      </div>
    );
  }
  
}

export default Home;
