import React, { Component } from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom';

import Home from './Home';
import SobreNos from './SobreNos';
import Servicos from './Servicos';
import Contato from './Contato';

import './css/reset.css'
import './App.css'; 
import './css/grid.css'
import './css/banner.css'

class App extends Component {
  
  render() {
    return (
      <div className="App">
        <Router>
          <Route path="/" exact component={Home} />
          <Route path="/sobre-nos" component={SobreNos} /> 
          <Route path="/servicos" component={Servicos} /> 
          <Route path="/contato" component={Contato} /> 
        </Router>
      </div>
    );
  }
  
}

export default App;
