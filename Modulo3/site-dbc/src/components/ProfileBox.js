import React, {Component} from 'react';
import ProfileImage from './ProfileImage';
import DefaultButton from './DefaultButton';
import '../css/profile.css'

export default class ProfileBox extends Component {

    render() {
      return (
        <article class="profile-box">
            <ProfileImage/>
            <h3 class="profile-name">Nome</h3> 
            <p class="description">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quibusdam itaque cum fugiat amet magni beatae, ullam ratione incidunt accusamus placeat nam voluptatem dicta dignissimos nesciunt autem dolorum consequatur modi qui.</p>
            <DefaultButton buttonClass="button-blue" text="Adote-me"/>
        </article>
      );
    }
      
}
