import React, {Component} from 'react';
import sad from '../img/sad-boi.jpeg'
import '../css/profile.css'

export default class ProfileImage extends Component {

  render() {
    return (
        <div className="profile-pic-box">
            <img src={sad} alt="Imagem de perfil" class="profile-pic"/>
        </div>
    );
  } 
}