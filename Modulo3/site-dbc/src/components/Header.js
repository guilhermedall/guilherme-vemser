import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import logo from '../img/logo-dbc-topo.png'
import '../css/header.css'

export default class Header extends Component {
    render() {
        return (
            <header className="main-header">
                <nav className="container clearfix">
                    <Link className="logo" to="/">
                        <img src={logo} alt="DBC Logo"/>
                    </Link>
                    <label className="mobile-menu" for="mobile-menu">
                        <span></span>
                        <span></span>
                        <span></span>
                    </label>
                    <input id="mobile-menu" type="checkbox"/>

                    <ul className="clearfix">
                        <li>
                            <Link className="link" to="/">Home</Link>
                        </li>
                        <li>
                            <Link className="link" to="/sobre-nos">Sobre Nós</Link>
                        </li>
                        <li>
                            <Link className="link" to="/servicos">Serviços</Link>
                        </li>
                        <li>
                            <Link className="link" to="/contato">Contato</Link>
                        </li>
                    </ul>
                </nav>
            </header>
        )
    }
}
    