import React, {Component} from 'react';
import '../css/map.css'

export default class DefaultButton extends Component {

  render() {
    return (
        <div class="map-container">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3454.7202462825653!2d-51.170870285284664!3d-30.01618823692249!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x951977775fc4c071%3A0x6de693cbd6b0b5e5!2sDBC%20Company!5e0!3m2!1spt-BR!2sbr!4v1576787367964!5m2!1spt-BR!2sbr" allowfullscreen=""></iframe>
        </div>
    );
  }
}