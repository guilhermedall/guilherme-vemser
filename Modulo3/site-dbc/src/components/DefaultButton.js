import React, {Component} from 'react';
import '../css/button.css'

export default class DefaultButton extends Component {

  render() {
    return (
      <React.Fragment>
        <a className={`button ${this.props.buttonClass}` } href="#">{this.props.text}</a>
      </React.Fragment>
    );
  }
}