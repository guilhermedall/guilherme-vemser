import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import '../css/footer.css'

export default class Footer extends Component {

  render() {
    return (
      <footer className="main-footer">
          <nav className="container">
            <ul className="">
                <li>
                  <Link className="link" to="/">Home</Link>
                </li>
                <li>
                  <Link className="link" to="/sobre-nos">Sobre Nós</Link>
                </li>
                <li>
                  <Link className="link" to="/servicos">Serviços</Link>
                </li>
                <li>
                  <Link className="link" to="/contato">Contato</Link>
                </li>
            </ul>
         </nav>
         <p>
             &copy;Copyright DBC Company - 2019
         </p>
      </footer>
    );
  }
}