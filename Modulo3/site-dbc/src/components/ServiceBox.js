import React, {Component} from 'react';
import ProfileImage from './ProfileImage';
import '../css/servicos.css'

export default class ServiceBox extends Component {

  render() {
    return (
        <article class="service-box">
            <ProfileImage/>
            <h3 class="service-name">Nome</h3> 
            <p class="description">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quibusdam itaque cum fugiat amet magni beatae, ullam ratione incidunt accusamus placeat nam voluptatem dicta dignissimos nesciunt autem dolorum consequatur modi qui.</p>
        </article>
    );
  }
}