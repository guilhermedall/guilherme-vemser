import React, {Component} from 'react';

export default class FormInput extends Component {

  render() {
    return (
      <React.Fragment>
        <label for="">{this.props.titulo}</label>
        <input type="text" placeholder={this.props.placeholder}/>
      </React.Fragment>
    );
  }
}