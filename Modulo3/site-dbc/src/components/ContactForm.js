import React, {Component} from 'react';
import FormInput from './FormInput'
import DefaultButton from './DefaultButton'
import '../css/form.css'

export default class ContactForm extends Component {

  render() {
    return (
      <form class="contact-form" action="">
          <FormInput titulo="Nome" placeholder="Seu nome e sobrenome"/>
          <FormInput titulo="E-mail" placeholder="Seu e-mail"/>
          <FormInput titulo="Telefone" placeholder="Seu número de telefone"/>
          <label for="">Mensagem</label>
          <textarea placeholder="Escreva aqui sua mensagem" class="" name="" id="" cols="34" rows="10"/>
          <DefaultButton text="Enviar"/>
      </form>
    );
  }
}