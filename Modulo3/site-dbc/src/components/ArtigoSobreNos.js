import React, {Component} from 'react';
import docDog from '../img/doc-dog.jpg'
import '../css/sobre-nos.css'

export default class ArtigoSobreNos extends Component {

  render() {
    return (
        <div className={`${this.props.posicaoFoto} row`}>
            <img src={docDog} alt="Foto do Artigo" className="col col-3"/>
            <article className="col col-9 bg-gray">
                <h2>Título</h2>
                <p>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Adipisci amet soluta culpa et laboriosam quidem autem delectus voluptatum, neque nostrum nihil, minima, ipsa quaerat laborum perspiciatis ab cupiditate quos eum.
                </p>
            </article>
        </div>
    );
  }
}