/* let vemSer = {
    local: "DBC",
    ano: "2020",
    imprimirInformacoes: function(quantidade, modulo) {
        console.log(`Argumentos: ${this.local} | ${this.ano}
        | ${quantidade} | ${modulo}`) 
    }
};

vemSer.imprimirInformacoes(20, 3); */

/* class Jedi {
    constructor(nome) {
        this.nome = nome;
    }

    atacarComSabre() {
        setTimeout(() => {
            console.log(`${this.nome} atacou com o sabre!`)
        }, 500);
    }

    atacarComSabreSelf() {
        let self = this;
        setTimeout(function() {
            console.log(`${self.nome} atacou com o sabre de novo!`)
        }, 1000);
    }
}

let luke = new Jedi("Luke");
luke.atacarComSabre();
luke.atacarComSabreSelf();
console.log(luke) */

/* let defer = new Promise((resolve, reject) => {
    setTimeout(() => {
        if (true) {
            resolve("Foi resolvido");
        } else {
            reject("Erro")
        }
    }, 2000);
});

defer
    .then((data) => {
        console.log(data)
        return "Novo resultado"
    })
    .then((data) => console.log(data))
    .catch((erro) => console.log(erro)); */

let pokemon = fetch(`https://pokeapi.co/api/v2/pokemon/`);

pokemon
    .then(data => data.json())
    .then(data => console.log(data.results))

